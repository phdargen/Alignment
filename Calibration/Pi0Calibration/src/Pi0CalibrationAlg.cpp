/*
 * =====================================================================================
 *
 *       Filename:  Pi0CalibrationAlg.cpp
 *
 *    Description:  main algorithm for pi0 calibration at LHCb online
 *
 *        Version:  1.0
 *        Created:  11/21/2016 01:53:44 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>
#include <fstream>

#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticlePropertySvc.h"

#include "CaloDet/DeCalorimeter.h"
#include "Kernel/CaloCellCode.h"
#include "CaloKernel/CaloVector.h"
#include "CaloDet/CellParam.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiAlg/GaudiAlg.h"

// from local
#include "Pi0Calibration/Pi0CalibrationAlg.h"
#include "Pi0Calibration/Pi0MassFiller.h"
#include "Pi0Calibration/Pi0MassFitter.h"
#include "Pi0Calibration/Pi0LambdaMap.h"

#include "TRandom.h"
#include "TFile.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TGraph2D.h"
#include "TColor.h"
#include "TStyle.h" 
#include "TROOT.h" 
#include "TText.h" 
#include "TBox.h" 
#include "TArrow.h"
#include "TMath.h"
#include "TPostScript.h"

#include "RooRealVar.h"
#include "RooDataSet.h"

//from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;

DECLARE_ALGORITHM_FACTORY(Pi0CalibrationAlg)

Pi0CalibrationAlg::Pi0CalibrationAlg(const std::string& name, ISvcLocator* pSvcLocator)
       : GaudiAlgorithm( name, pSvcLocator ) 
{
       
  declareProperty( "tupleFileName", m_tupleFileName);
  declareProperty(     "tupleName", m_tupleName);
  declareProperty(     "outputDir", m_outputDir);
  declareProperty("lambdaFileName", m_lambdaFileName="");
  declareProperty("saveLambdaFile", m_saveLambdaFile);
  declareProperty(     "fitMethod", m_fitMethod="CHI2");
  declareProperty(      "filetype", m_filetype="ROOT");

}

Pi0CalibrationAlg::~Pi0CalibrationAlg(){ }

StatusCode Pi0CalibrationAlg::initialize(){
  
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() ) return sc;

  // find the pi0 mass value
  const LHCb::IParticlePropertySvc* ppsvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc" ) ;
  const LHCb::ParticleProperty* pi0 = ppsvc->find ( "pi0" ) ;
  if ( 0 == pi0 ) {return sc;} 
  m_pdgPi0 = pi0->mass();
  // import a list of calo cells
  DeCalorimeter* m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  const CaloVector<CellParam>& cells = m_calo->cellParams();
  for( CaloVector<CellParam>::const_iterator icell = cells.begin(); icell != cells.end(); icell++ ){
    LHCb::CaloCellID id = (*icell).cellID();
    if( !m_calo->valid( id ) || id.isPin() ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::Shifted) ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::VeryShifted) ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::StuckADC) ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::OfflineMask) ) continue;
    m_cells.push_back( id );
    m_indices.push_back( id.index() );
    //m_map[ id.areaName() ][ id.index() ]  = icell->center();
    //m_cellsize[ id.areaName() ] = icell->size();
  }

  return sc;
}

StatusCode Pi0CalibrationAlg::execute(){
  StatusCode sc = StatusCode::SUCCESS;
  return sc;
}

StatusCode Pi0CalibrationAlg::finalize(){
  StatusCode sc = StatusCode::SUCCESS;

  if( !exists(m_tupleFileName) ) return StatusCode::FAILURE;
  // load the lambda map
  Pi0LambdaMap lMap( m_indices );
  if( exists(m_lambdaFileName) ) lMap.loadMapFromFile( m_lambdaFileName );
  if(msgLevel(MSG::INFO)) info() << " the lambda is loaded from file " << m_lambdaFileName << endmsg;
  // load the histogram
  Pi0MassFiller filler;
  if( m_filetype=="MMAP" ){
    if( msgLevel(MSG::INFO) ) info() << " read files created by MMapVector" << endmsg;
    filler =  Pi0MassFiller( m_tupleFileName, m_indices, lMap );
  } else if( m_filetype=="ROOT" ){
    if( msgLevel(MSG::INFO) ) info() << " read normal ROOT files" << endmsg;
    filler =  Pi0MassFiller( m_tupleFileName, m_tupleName, m_indices, lMap );
  } else if( m_filetype=="TH2D" ){
    filler = Pi0MassFiller( m_tupleFileName );
  }else{
    err() << " File type not recognised " << m_filetype << endmsg;
    return StatusCode::FAILURE;
  }
  TH2* m_hists = filler.hists();
  TH2* m_hists_bg = filler.histsBG();

  std::map<unsigned int, std::pair<double,double> > meanValues;
  std::map<unsigned int, std::pair<double,double> > sigmaValues;
  meanValues.clear();
  sigmaValues.clear();
  m_skippedCells.clear();
  
  if(msgLevel(MSG::INFO)) info() << " fit the pi0 mass distribution to get the initial lambda values and the resolutions in each region " << endmsg; 
  if(msgLevel(MSG::VERBOSE)) verbose() << " create new maps for all the cells " << endmsg;

  Pi0LambdaMap newMap( m_indices );
  for( auto& cell : m_cells ) {
    unsigned int seedindex = cell.index();

    if(msgLevel(MSG::VERBOSE)) verbose()<< "create histogram for cell " << seedindex << " " << cell.areaName() << " COL" << cell.col() << " ROW" << cell.row() << endmsg;

    TH1D* hist    = m_hists->ProjectionY(  ("S_ID"+std::to_string(seedindex)+"_ROW"+std::to_string(cell.row())+"_COL"+std::to_string(cell.col())).c_str(), seedindex+1, seedindex+1 );
    TH1D* hist_bg = m_hists_bg->ProjectionY(  ("B_ID"+std::to_string(seedindex)+"_ROW"+std::to_string(cell.row())+"_COL"+std::to_string(cell.col())).c_str(), seedindex+1, seedindex+1 );
    if( hist->GetEntries()==0 ) {m_skippedCells.push_back(cell); continue;}

    Pi0MassFitter fitter( hist, hist_bg, m_pdgPi0, 10.0 );
    std::string outfileName( "Calibration_Pi0_massfit");
    outfileName += "_"+cell.areaName();
    outfileName += "_ROW"+std::to_string( cell.row() );
    outfileName += "_COL"+std::to_string( cell.col() );
    outfileName += "_INDEX"+std::to_string( cell.index() );
    outfileName += ".pdf";
    if( m_fitMethod=="LIKELIHOOD")
	fitter.likelihoodfit( m_outputDir+"/"+cell.areaName(), outfileName, msgLevel(MSG::VERBOSE) );
    if( m_fitMethod=="CHI2")
	fitter.chi2fit( m_outputDir+"/"+cell.areaName(), outfileName, msgLevel(MSG::VERBOSE) );

    meanValues[cell.index()]   = fitter.getMean();
    sigmaValues[cell.index()]  = fitter.getSigma();
    
    auto lold   = lMap.get_lambda( cell.index() );
    if( !fitter.getStatus() ){
      if( lold.first < 0.0) lold = std::make_pair(1.0, 0.0);
      auto lnew_value = m_pdgPi0/fitter.getMean().first;
      auto lnew_error = lnew_value*fitter.getMean().second/fitter.getMean().first;
      auto lambda_value = lold.first * lnew_value;
      auto lambda_error = lambda_value * std::sqrt( lnew_error/lnew_value*lnew_error/lnew_value+lold.second/lold.first*lold.second/lold.first  );
      newMap.setLambda( cell.index(), std::make_pair( lambda_value,lambda_error ) );
    }else{ 
      m_skippedCells.push_back( cell );
      if( !( lold.first<0.0 || lold.second<0.0) ) newMap.setLambda( cell.index(), lold );
    }
  }
  
  if(msgLevel(MSG::INFO)) info() << " save the updated lambda to the file " << m_saveLambdaFile << endmsg; 
  newMap.saveMapToFile( m_saveLambdaFile );

  // create monitoring plots for quality checks
  //monitorPlots(  "sigma", 10.0, sigmaValues );
  //monitorPlots( "lambda",  1.0, lambdaValues);
  saveCells( m_skippedCells );
  saveValues(  meanValues, "mean" );
  saveValues( sigmaValues, "sigma" );
  meanValues.clear();
  sigmaValues.clear();
  return sc;
}

void Pi0CalibrationAlg::saveCells( const std::vector<LHCb::CaloCellID>& cells ){
  boost::filesystem::path filename( m_outputDir+"/"+"skipped_cells.txt" );
  std::ofstream ofile;
  ofile.open( filename.string(), std::ios::out | std::ios::trunc  );
  for( auto& cell : cells ) ofile << cell.calo() << " " << cell.area() << " " << cell.row() << " " << cell.col() << " " << cell.index() << "\n";
  ofile.close();
}
void Pi0CalibrationAlg::saveValues( const std::map<unsigned int, std::pair<double,double> >& values, std::string varname ){
  boost::filesystem::path filename( m_outputDir+"/"+varname+".txt" );
  std::ofstream ofile;
  ofile.open( filename.string(), std::ios::out | std::ios::trunc  );
  for( auto& value : values ) 
    ofile << value.first << "  " << value.second.first << "  " << value.second.second << "\n";
  ofile.close();
}

/*  
void Pi0CalibrationAlg::monitorPlots( std::string name, double centreVal, const std::map<unsigned int, double>& values, bool ownRange  ){

  std::map< std::string, std::vector<double> > xvalues;
  std::map< std::string, std::vector<double> > yvalues;
  for(auto const& igrid : m_map ){
    auto area = igrid.first;
    for(auto value : igrid.second ){
      xvalues[area].push_back( value.second.x()-0.5*m_cellsize[area] );
      yvalues[area].push_back( value.second.y()-0.5*m_cellsize[area]  );
    }
  }
  std::vector<std::string> areas = {"Inner", "Middle", "Outer"};
  std::map<std::string, TH2*> hist_calo;
  for(auto area : areas ){
    std::sort( xvalues[area].begin(), xvalues[area].end() );
    std::sort( yvalues[area].begin(), yvalues[area].end() );
    xvalues[area].push_back( xvalues[area].back() + m_cellsize[area]);
    yvalues[area].push_back( yvalues[area].back() + m_cellsize[area]);
    xvalues[area].erase( std::unique(xvalues[area].begin(), xvalues[area].end()), xvalues[area].end());
    yvalues[area].erase( std::unique(yvalues[area].begin(), yvalues[area].end()), yvalues[area].end());
    hist_calo[area] = new TH2D(("hist_calo_"+name+"_"+area).c_str(), ("hist_calo_"+name+"_"+area).c_str(), xvalues[area].size()-1, xvalues[area].data(), yvalues[area].size()-1, yvalues[area].data() );
  }
  std::map<std::string, TH2*> grid_calo;
  for( auto area : areas ){
    std::vector<double> cols;
    std::vector<double> rows;
    for( auto cell : m_cells ) {
      if( cell.areaName() != area ) continue;
      cols.push_back( cell.col()-0.5 );
      rows.push_back( cell.row()-0.5 );
    }
    std::sort( cols.begin(), cols.end() );
    std::sort( rows.begin(), rows.end() );
    cols.push_back( cols.back()+1.0);
    rows.push_back( rows.back()+1.0);
    grid_calo[area] = new TH2D(("grid_calo_"+name+"_"+area).c_str(), ("grid_calo_"+name+"_"+area).c_str(), cols.size()-1, cols.data(), rows.size()-1, rows.data() );
  }

  std::map<std::string, std::pair<double,double> > min_max;
  min_max["Inner"]  = std::make_pair(0., 0.);
  min_max["Middle"] = std::make_pair(0., 0.);
  min_max["Outer"]  = std::make_pair(0., 0.);
  for(auto cell : m_cells){

    unsigned int index = cell.index();
    hist_calo[cell.areaName()]->Fill( m_map[cell.areaName()][index].x(), m_map[cell.areaName()][index].y(), values.find(index)->second );
    grid_calo[cell.areaName()]->Fill( cell.col(), cell.row(), values.find(index)->second );

    if( min_max[cell.areaName()].first  > values.find(index)->second ) min_max[cell.areaName()].first  = values.find(index)->second;
    if( min_max[cell.areaName()].second < values.find(index)->second ) min_max[cell.areaName()].second = values.find(index)->second;

  }

  // build 2D histogram to visualize the mean and sigma   
  gStyle->SetOptStat(0);   
  gStyle->SetOptFit(0);   
  gStyle->SetPadRightMargin(0.25);   
  gStyle->SetTitleX(0.5);   
  gStyle->SetTitleAlign(23);   
  gStyle->SetTitleBorderSize(0);   
  gStyle->SetPaintTextFormat("5.0f");   
  gStyle->SetStatFormat("5.5f");   
  gStyle->SetTitleFontSize(0.07);   
  gStyle->SetPadTickY(1);   
  gStyle->SetPadTickX(1); 
    
  unsigned int nColors=52;   
  Int_t MyPalette[52];   
  Double_t s[3] = {0.00, 0.50, 1.00};   
  Double_t b[3] = {0.80, 1.00, 0.00};   
  Double_t g[3] = {0.00, 1.00, 0.00};   
  Double_t r[3] = {0.00, 1.00, 0.80};   
  Int_t FI = TColor::CreateGradientColorTable(3, s, r, g, b, nColors);   
  for (unsigned int k(0); k < nColors; k++){     MyPalette[k] = FI+k;   }   
  gStyle->SetNumberContours(nColors);   
  gStyle->SetPalette(nColors, MyPalette);  
  
  gROOT->ForceStyle();

  // find the minimum and maximum of the values in all the areas
  auto it = std::minmax_element(values.begin(), values.end(), [](const std::map<unsigned int,double>::value_type& l, const std::map<unsigned int,double>::value_type& r)->bool{return l.second<r.second;} );
  double deltaVal = 0.5*(it.second->second - it.first->second);
  if( centreVal < it.first->second ) centreVal = it.first->second+deltaVal;
  if( centreVal > it.second->second ) centreVal = it.second->second-deltaVal;

  TCanvas* cc = new TCanvas("cc", "cc", 1600, 1200);
  for(auto hist : hist_calo){
    hist.second->SetMaximum( centreVal+deltaVal );
    if( centreVal-deltaVal > 0.)
      hist.second->SetMinimum( centreVal-deltaVal );
    else if( it.first->second >0. )
      hist.second->SetMinimum( it.first->second );
    else
      hist.second->SetMinimum( 0. );
    hist.second->SetTitle("ECAL cells X vs Y");
    hist.second->GetXaxis()->SetTitle("X [mm]");
    hist.second->GetYaxis()->SetTitle("Y [mm]");
  }
  hist_calo.find("Outer")->second->Draw("COLZ");
  hist_calo.find("Middle")->second->Draw("COLZ,SAME");
  hist_calo.find("Inner")->second->Draw("COLZ,SAME");
  // plot boxes
  TBox* box = new TBox();
  box->SetFillColor(kWhite);   
  box->SetFillStyle(0);   
  box->SetLineStyle(1);   
  box->SetLineColor(kBlack); //14   
  for(auto const& igrid : m_map ){
    auto area = igrid.first;
    for(auto value : igrid.second ){
      box->DrawBox( value.second.x()-0.5*m_cellsize[area], value.second.y()-0.5*m_cellsize[area], value.second.x()+0.5*m_cellsize[area], value.second.y()+0.5*m_cellsize[area]);
    }
  }
  boost::filesystem::path outfile_calomean( m_outputDir+"/"+"calo_"+name+".pdf" );
  cc->SaveAs( outfile_calomean.c_str() );
  cc->Close();
  for(auto hist : grid_calo){
    auto area = hist.first;
    TCanvas* cc = new TCanvas("cc", "cc", 1600, 1200);
    if( ownRange ){
      hist.second->SetMaximum( min_max[area].second);
      if( min_max[area].first > 0. )
        hist.second->SetMinimum( min_max[area].first);
      else
        hist.second->SetMinimum( 0. );
    }else{
      hist.second->SetMaximum( centreVal+deltaVal );
      if( centreVal-deltaVal > 0.)
        hist.second->SetMinimum( centreVal-deltaVal );
      else if( it.first->second >0. )
        hist.second->SetMinimum( it.first->second );
      else
        hist.second->SetMinimum( 0. );
    }
    hist.second->SetTitle( area.c_str() );
    hist.second->Draw("COLZ");
    hist.second->GetXaxis()->SetTitle("COLUMN");
    hist.second->GetYaxis()->SetTitle("ROW");
    // plot boxes
    TBox* box = new TBox();
    box->SetFillColor(kWhite);   
    box->SetFillStyle(0);   
    box->SetLineStyle(3);   
    box->SetLineColor(kBlack); //14   
    for( auto cell : m_cells ) {
      if( cell.areaName()==area ) box->DrawBox( cell.col()-0.5, cell.row()-0.5, cell.col()+0.5, cell.row()+0.5);
    }
    delete box;
    TBox* boxempty = new TBox();   
    boxempty->SetFillColor(14);  
    boxempty->SetFillStyle(3254);  
    boxempty->SetLineStyle(3);   
    boxempty->SetLineColor(14);
    //boxempty->SetLineStyle(1);   
    //boxempty->SetLineColor(kOrange);
    for( auto cell : m_skippedCells) {
      if( cell.areaName()==area ) boxempty->DrawBox( cell.col()-0.5, cell.row()-0.5, cell.col()+0.5, cell.row()+0.5);
    }
    delete boxempty;
    boost::filesystem::path outfile_calogrid( m_outputDir+"/"+"calo_"+name+"_grid_"+area+".pdf" );
    cc->SaveAs( outfile_calogrid.c_str() );
    cc->Close();
  }
  for( auto hist:hist_calo){
    delete hist.second;
  }
  for( auto hist:grid_calo){
    delete hist.second;
  }
}

*/


