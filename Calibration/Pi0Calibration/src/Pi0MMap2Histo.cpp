/*
 * =====================================================================================
 *
 *       Filename:  Pi0MMap2Histo.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/27/2017 05:05:55 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>
#include <cstdint>

#include "Kernel/ParticleProperty.h"
#include "Kernel/IParticlePropertySvc.h"

#include "CaloDet/DeCalorimeter.h"
#include "Kernel/CaloCellCode.h"
#include "Kernel/CaloCellID.h"
#include "CaloKernel/CaloVector.h"
#include "CaloDet/CellParam.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiAlg/GaudiAlg.h"

#include "TFile.h"
#include "TH2.h"
#include "TTree.h"

//from local
#include "Pi0Calibration/MMapVector.h"
#include "Pi0Calibration/Pi0CalibrationFile.h"
#include "Pi0Calibration/Pi0LambdaMap.h"
#include "Pi0Calibration/Pi0MMap2Histo.h"
#include "Pi0Calibration/Pi0MassFiller.h"
//from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;

DECLARE_ALGORITHM_FACTORY(Pi0MMap2Histo)

Pi0MMap2Histo::Pi0MMap2Histo( const std::string& name, ISvcLocator* pSvcLocator ) : GaudiAlgorithm( name, pSvcLocator ){

  declareProperty(     "filenames", m_filenames  );
  declareProperty(     "outputDir", m_outputDir  );
  declareProperty(    "outputName", m_outputName );
  declareProperty(       "nworker", m_nworker    );
  declareProperty("lambdaFileName", m_lambdaFileName="");

}

Pi0MMap2Histo::~Pi0MMap2Histo(){}

StatusCode Pi0MMap2Histo::initialize(){
  
  StatusCode sc = GaudiAlgorithm::initialize();
  if( sc.isFailure() ) return sc;

  // import a list of calo cells
  DeCalorimeter* m_calo = getDet<DeCalorimeter>( DeCalorimeterLocation::Ecal );
  const CaloVector<CellParam>& cells = m_calo->cellParams();
  for( CaloVector<CellParam>::const_iterator icell = cells.begin(); icell != cells.end(); icell++ ){
    LHCb::CaloCellID id = (*icell).cellID();
    if( !m_calo->valid( id ) || id.isPin() ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::Shifted) ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::VeryShifted) ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::StuckADC) ) continue;
    if( m_calo->hasQuality(id, CaloCellQuality::OfflineMask) ) continue;
    m_indices.push_back( id.index() );
  }
  for( auto index : m_indices ) std::cout << "  " << index ;
  std::cout << std::endl;

  return sc;
}

StatusCode Pi0MMap2Histo::execute(){
  StatusCode sc = StatusCode::SUCCESS;

  return sc;
}

StatusCode Pi0MMap2Histo::finalize(){
  StatusCode sc = StatusCode::SUCCESS;

  info() << "Pi0MMap2Histo : START  " << endmsg;

  std::vector< std::vector<unsigned int> > subtasks( m_nworker );
  for( auto& c:subtasks ) c.reserve( m_indices.size()/m_nworker );
  info() << "total cells to be processed by each worker " << m_indices.size()/m_nworker << endmsg;

  int i=0;
  for( auto index : m_indices ) subtasks[i++%m_nworker].push_back( index );

  auto  min_max = std::minmax_element( m_indices.begin(), m_indices.end() );
  int xmax = min_max.second-m_indices.begin();

  Pi0LambdaMap lambdamap( m_indices );
  if( exists(m_lambdaFileName) ) lambdamap.loadMapFromFile( m_lambdaFileName );
  if(msgLevel(MSG::INFO)) info() << " the lambda is loaded from file " << m_lambdaFileName << endmsg;

  int iwoker = 1;
  for( auto& subtask : subtasks ){
    info() << " start worker " << iwoker << " for the following indices: "<< endmsg;
    for( auto index : subtask ) std::cout << "  " << index ;
    std::cout << std::endl;

    auto sel = Pi0CalibrationFile::make_indices_selector( subtask );
    info() << "make_indices_selector called successfully " << endmsg;

    info() << " a list of file names " << endmsg;
    for( auto& filename : m_filenames ) info() << "  file : " << filename << endmsg;

    boost::filesystem::path outfilename( m_outputDir+"/"+m_outputName+"_Worker"+std::to_string(iwoker)+".root" );
    TFile *fout = new TFile( outfilename.c_str(), "RECREATE" );
    TH2D *hists    = new TH2D("hists",    "hists",    m_indices.at(xmax), 0, m_indices.at(xmax), 100, 0.0, 250.0);
    TH2D *hists_bg = new TH2D("hists_bg", "hists_bg", m_indices.at(xmax), 0, m_indices.at(xmax), 100, 0.0, 250.0);
    //hists->SetNameTitle( "hists", "hists");
    //hists_bg->SetNameTitle( "hists_bg", "hists_bg");
    for( auto& filename : m_filenames ){
      if( !exists(filename) ){ 
	err() << "file not exist! "<< filename << endmsg; 
	continue;
      }
      info() << "start to read file from " << filename << endmsg;
      MMapVector<Candidate> vorigin(filename.c_str(), MMapVector<Candidate>::ReadOnly);
      info() << "open the file successfully with " << vorigin.size() << " entries " << endmsg;
      for( auto& c : vorigin ){
        auto bkg  = c.bkg; 
        auto ind1 = c.ind1;
        auto ind2 = c.ind2;
        auto m12  = c.m12;
        // correct the energy and re-computer the pi0 invariant mass
	/*  	
        auto prs1 = c.prs1;
        auto prs2 = c.prs2;
        auto g1E  = c.g1E;
        auto g2E  = c.g2E;
        double scale = 1.;
        std::pair<double,double> lambda1 = lambdamap.get_lambda( ind1 );
        std::pair<double,double> lambda2 = lambdamap.get_lambda( ind2 );
        if( lambda1.first<0.0 ) lambda1 = std::make_pair( 1.0, 0.0);
        if( lambda2.first<0.0 ) lambda2 = std::make_pair( 1.0, 0.0);
        scale = std::sqrt( lambda1.first * lambda2.first );
        if( prs1>10.0 && prs2>10.0 ) scale = std::sqrt(lambda1.first+(1.0-lambda1.first)*prs1/g1E)*std::sqrt(lambda2.first+(1.0-lambda2.first)*prs2/g2E);
        if( prs1>prs2 && prs1>10.0 && prs2<10.0 ) scale = std::sqrt(lambda1.first+(1.0-lambda1.first)*prs1/g1E)*std::sqrt(lambda2.first);
        if( prs1<prs2 && prs1<10.0 && prs2>10.0 ) scale = std::sqrt(lambda1.first)*std::sqrt(lambda2.first+(1.0-lambda2.first)*prs2/g2E);
        */
	auto scale = Pi0MassFiller::scale_factor(c, lambdamap);
	//select the cells to fill in the histograms
	if( sel(ind1) ){ 
	  //auto scale = scale_factor( c, lambdamap );
          if(bkg==0) hists->Fill( ind1, m12*scale );
          if(bkg==1) hists_bg->Fill( ind1, m12*scale );
	}
	if( sel(ind2) ){ 
	  //auto scale = scale_factor( c, lambdamap );
          if(bkg==0) hists->Fill( ind2, m12*scale );
          if(bkg==1) hists_bg->Fill( ind2, m12*scale );
	}
      }
      info() << "finished reading file " << filename << endmsg;
    }// all input files
    hists->Write();
    hists_bg->Write();
    fout->Close();
    //hists->Clear();
    //hists_bg->Clear();

    //info() << "saved MMap file for worker " << iwoker << endmsg;
    info() << "saved histograms for worker " << iwoker << endmsg;

    iwoker++;
  }// all the workers

  return sc;
}


