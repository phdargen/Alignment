/*
 * =====================================================================================
 *
 *       Filename:  Pi0MassFitter.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/21/2016 04:40:34 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */


//from ROOT
#include "TFile.h"
#include "TTree.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TSpectrum.h"
#include "TF1.h"
#include "TMath.h"
#include "TStyle.h"
#include "TError.h"
#include "TFitResult.h"
#include "TFitResultPtr.h"
//from RooFit
#include "RooDataHist.h"
#include "RooArgList.h"
#include "RooRealVar.h"
#include "RooChebychev.h"
#include "RooGaussian.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooFormulaVar.h"
#include "RooGenericPdf.h"
#include "RooProdPdf.h"

//from local
#include "Pi0Calibration/Pi0MassFitter.h"

//from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace RooFit;
using namespace Calibration::Pi0Calibration;


Pi0MassFitter::Pi0MassFitter(TH1* hist, TH1* hist_bg, double mpi0, double sigma){
  m_hist    = hist;
  m_hist_bg = hist_bg;
  m_mpi0    = mpi0;
  m_sigma0 = sigma;

  m_mean  = std::make_pair(0.,0.);
  m_sigma = std::make_pair(0.,0.);
  m_status= -1;
}

void Pi0MassFitter::chi2fit( const std::string& outputdir, const std::string& outfilename, bool verbose){
  std::string subdir = "good";

  gErrorIgnoreLevel = kError;

  double massMin = m_hist->GetXaxis()->GetXmin();
  double massMax = m_hist->GetXaxis()->GetXmax();
  double binW    = m_hist->GetXaxis()->GetBinWidth(1);
  double nSEvts  = m_hist->GetEntries()/binW;
  double nBEvts  = m_hist_bg->GetEntries()/binW;
  double fs      = nSEvts/(nBEvts+nSEvts);
  if( fs<0.0 ) fs = 0.10;

  if( m_hist->GetEntries() < 2500 ){
    subdir = "nofit";

    gStyle->SetOptFit(1112);
    gStyle->SetStatFormat("6.2g");
    gStyle->SetStatStyle(4001);
    TCanvas* cc = new TCanvas();
    m_hist->Draw();
    m_hist_bg->SetLineColor(6);
    m_hist_bg->SetMarkerColor(6);
    m_hist_bg->SetMarkerStyle(24);
    m_hist_bg->Draw("same");

    if( !exists(outputdir) ) create_directories( outputdir );
    if( !exists(outputdir+"/"+subdir) ) create_directories( outputdir+"/"+subdir );
    boost::filesystem::path outfile(outputdir+"/"+subdir+"/"+outfilename);

    cc->SaveAs( outfile.c_str() );
    cc->Close();
    return;
  }

  auto background = [](double *x, double *par){
    double xx   = x[0];
    double dmb  = -1.0+2.0*(xx-par[3])/(par[4]-par[3]);
    double background = 1.0+par[1]*dmb+par[2]*(2.0*dmb*dmb-1);
    return par[0]*background;
  };

  TF1 *model_bkg = new TF1("model_bkg",background, massMin, massMax, 5);
  model_bkg->SetParNames("Nevts", "a", "b", "xmin", "xmax");
  model_bkg->SetParameters(nBEvts, 1.0, -0.1);
  model_bkg->FixParameter(3,massMin);
  model_bkg->FixParameter(4,massMax);
  model_bkg->SetLineColor(3);
  model_bkg->SetLineStyle(2);
  m_hist_bg->Fit("model_bkg", "QS");
   
  // define the fitting functions
  auto userfunction = [](double *x, double *par){
    double xx     = x[0];
    double dm     = (xx - par[1])/par[2];
    double signal = 1.0/std::sqrt(2.0*TMath::Pi())/par[2]*std::exp( -0.5*dm*dm );
    double dmb    = -1.0+2.0*(xx-par[6])/(par[7]-par[6]);
    double background = 1.0+par[4]*dmb+par[5]*(2.0*dmb*dmb-1);
    double xrange = par[7]-par[6];
    double total  = (1-par[5]/3.0)*xrange;
    return par[0]*( par[3]*signal + (1-par[3])*background/total );
  };

  TF1 *model = new TF1("model",userfunction,massMin, massMax,8);
  model->SetParNames("Nevts", "mean", "sigma", "fs", "a", "b", "xmin", "xmax");
  model->SetParameters(nSEvts, m_mpi0, m_sigma0, fs, model_bkg->GetParameter(1), model_bkg->GetParameter(2));
  if( m_mpi0-50.0 > massMin )
    model->SetParLimits(1, m_mpi0-50.0, m_mpi0+50.0);
  else
    model->SetParLimits(1, massMin, m_mpi0+50.0);
  model->SetParLimits(2, 5.0,25.0);
  model->SetParLimits(3, 0.0,1.0);
  model->FixParameter(6,massMin);
  model->FixParameter(7,massMax);
  TFitResultPtr fitres = m_hist->Fit(model,"SQ");

  // recipe from Philippe Ghez
  auto background_update = [](double *x, double *par){
    double xx   = x[0];
    double dmb  = -1.0+2.0*(xx-par[3])/(par[4]-par[3]);
    double background = 1.0+par[1]*dmb+par[2]*(2.0*dmb*dmb-1);
    if( xx > 105.0 && xx < 165.0 ){ 
	TF1::RejectPoint();
	return 0.0;
    }
    return par[0]*background;
  };
  TF1 *model_bkg_u = new TF1("model_bkg_u",background_update, massMin, massMax, 5);
  model_bkg_u->SetParNames("Nevts", "a", "b", "xmin", "xmax");
  model_bkg_u->SetParameters(100.0, 1.0, -0.1);
  model_bkg_u->FixParameter(3,massMin);
  model_bkg_u->FixParameter(4,massMax);
  model_bkg_u->SetLineColor(1);
  model_bkg_u->SetLineStyle(4);

  if( !fitres->IsEmpty() && (
           ( fitres->Ndf()>0 && fitres->Chi2()/fitres->Ndf()>1.5 )
	|| ( !fitres->IsValid() )
	|| ( fitres->Prob()<1e-3 )
	   ) 
	  ){
    // fit with the background shape fixed
    model->FixParameter(4, model_bkg->GetParameter(1));
    model->FixParameter(5, model_bkg->GetParameter(2));
    fitres = m_hist->Fit("model","SQ");
    // fix the background to the shape from sidebands in signal histogram
    if( fitres->Ndf()>0 && fitres->Chi2()/fitres->Ndf()>1.5 ){
      m_hist->Fit("model_bkg_u", "QS");
      model->FixParameter(4, model_bkg_u->GetParameter(1));
      model->FixParameter(5, model_bkg_u->GetParameter(2));
      fitres = m_hist->Fit("model","SQ", "", massMin, massMax);
    }
    // fit in the smaller range with background shape free
    if( fitres->Ndf()>0 && fitres->Chi2()/fitres->Ndf()>1.5 ){
      massMin += 75.0;
      model_bkg->FixParameter(3,massMin);
      model_bkg->FixParameter(4,massMax);
      m_hist_bg->Fit("model_bkg","QS","",massMin,massMax);
      if( m_mpi0-50.0 > massMin )
        model->SetParLimits(1, m_mpi0-50.0, m_mpi0+50.0);
      else
	model->SetParLimits(1, massMin, m_mpi0+50.0);
      model->SetParLimits(2, 5.0, 50.0);
      model->ReleaseParameter(4);
      model->ReleaseParameter(5);
      model->SetParameter(4, model_bkg->GetParameter(1));
      model->SetParameter(5, model_bkg->GetParameter(2));
      model->FixParameter(6,massMin);
      model->FixParameter(7,massMax);
      fitres = m_hist->Fit("model","SQ", "", massMin, massMax);
    }
    // fix the background to the shape from background histogram
    if( fitres->Ndf()>0 && fitres->Chi2()/fitres->Ndf()>1.5 ){
      model->FixParameter(4, model_bkg->GetParameter(1));
      model->FixParameter(5, model_bkg->GetParameter(2));
      fitres = m_hist->Fit("model","SQ", "", massMin, massMax);
    }
    // fix the background to the shape from sidebands in signal histogram
    if( fitres->Ndf()>0 && fitres->Chi2()/fitres->Ndf()>1.5 ){
      model_bkg_u->FixParameter(3,massMin);
      model_bkg_u->FixParameter(4,massMax);
      m_hist->Fit("model_bkg_u", "QS");
      model->FixParameter(4, model_bkg_u->GetParameter(1));
      model->FixParameter(5, model_bkg_u->GetParameter(2));
      fitres = m_hist->Fit("model","SQ", "", massMin, massMax);
    }
    // high statistics but low signal fraction or bad resolution
    if( model->GetParameter(0)>5000 && (model->GetParameter(2)>20.0 || model->GetParameter(3)<0.015 ) ){
      model->SetParLimits(2, 6.5, 25.0);
      fitres = m_hist->Fit("model","SQ", "", massMin, massMax);
    }
  }

  m_mean  = std::make_pair( model->GetParameter(1), model->GetParError(1) );
  m_sigma = std::make_pair( model->GetParameter(2), model->GetParError(2) );
  if( model->GetParameter(2) <15.0 && model->GetParameter(2)>5.0 && model->GetParameter(3)>0.01 ) m_status = 0;
  else { verbose = true; subdir = "bad"; }
  if(verbose){
    gStyle->SetOptFit(1112);
    gStyle->SetStatFormat("6.2g");
    gStyle->SetStatStyle(4001);
    TCanvas* cc = new TCanvas();
    m_hist->Draw();
    m_hist_bg->SetLineColor(6);
    m_hist_bg->SetMarkerColor(6);
    m_hist_bg->SetMarkerStyle(24);
    m_hist_bg->Draw("same");

    if( !exists(outputdir) ) create_directories( outputdir );
    if( !exists(outputdir+"/"+subdir) ) create_directories( outputdir+"/"+subdir );
    boost::filesystem::path outfile(outputdir+"/"+subdir+"/"+outfilename);

    cc->SaveAs( outfile.c_str() );
    cc->Close();
  }

}

void Pi0MassFitter::likelihoodfit( const std::string& outputdir, const std::string& outfilename, bool verbose ){


  RooRealVar mass("mass", "mass", m_hist->GetXaxis()->GetXmin(), m_hist->GetXaxis()->GetXmax() );
  RooDataHist dataset( m_hist->GetName(), m_hist->GetTitle(), RooArgList(mass), m_hist );
  //dataset.Print();
  RooDataHist dataset_bg( m_hist_bg->GetName(), m_hist_bg->GetTitle(), RooArgList(mass), m_hist_bg );

  RooRealVar p0("p0", "p_{0}", 0.1, -1, 1);
  RooRealVar p1("p1", "p_{1}", 0.1, -10, 10);
  RooChebychev pdf_background( "pdf_background", "pdf_background", mass, RooArgList(p0, p1) );

  RooRealVar mu("mu", "#mu", m_mpi0, m_mpi0-50.0, m_mpi0+50.0);
  RooRealVar sigma("sigma", "#sigma", 10, 2, 25);
  RooGaussian pdf_signal( "pdf_signal", "pdf_signal", mass, mu, sigma );

  RooRealVar f_signal("f_signal", "f_{signal}", 0.2, 0, 1 );
  RooAddPdf model("model", "model", RooArgList(pdf_signal,pdf_background), RooArgList(f_signal));

  double massMin = m_hist->GetXaxis()->GetXmin();
  double massMax = m_hist->GetXaxis()->GetXmax();
  model.fitTo( dataset, Range(massMin,massMax) );

  if( sigma.getVal()<20. && sigma.getVal()>5.0 )  m_status = 0;

  m_mean = std::make_pair( mu.getVal(), mu.getError() );
  m_sigma= std::make_pair( sigma.getVal(), sigma.getError() );

  if( !exists(outputdir) ) create_directories( outputdir );
  boost::filesystem::path outfile(outputdir+"/"+outfilename);

  if( verbose ){
    TCanvas* cc = new TCanvas();
    RooPlot* frame = mass.frame( 50 );
    dataset.plotOn(frame);
    model.plotOn(frame);
    model.paramOn(frame, Layout(0.75, 0.99, 0.95), Format("NNEU",AutoPrecision(2)) );
    model.plotOn(frame,Components("pdf_signal"), LineColor(kRed) );
    model.plotOn(frame,Components("pdf_background"), LineColor(kGreen) );
    dataset_bg.plotOn(frame, MarkerColor(6), LineColor(6), MarkerStyle(24));
    frame->Draw();
    cc->SaveAs( outfile.c_str() );
    cc->Close();
  }
}
