/*
 * =====================================================================================
 *
 *       Filename:  Pi0CalibrationFile.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/15/2017 05:37:21 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */


#include <iostream>

//from ROOT
#include "TFile.h"
#include "TTree.h"
//from local
#include "Pi0Calibration/MMapVector.h"
#include "Pi0Calibration/Pi0CalibrationFile.h"
//from boost
#include <boost/filesystem.hpp>

using namespace boost;
using namespace boost::filesystem;
using namespace Calibration::Pi0Calibration;


template <class SEL>
Pi0CalibrationFile::Pi0CalibrationFile( const std::string& filename, const std::string& tuplename, const std::string& outfilename, const SEL& sel )
{
  if( !exists(filename) ){
    std::cout << "ERROR: file not exist! " << filename << std::endl;
    return;
  }
  TFile* file = new TFile( filename.c_str() );
  TTree* tree = (TTree*)file->Get( tuplename.c_str() );
  tree->SetBranchStatus("*", 0);
  for( auto name : {"bkg", "m12", "ind1", "ind2", "prs1", "prs2", "g1E", "g2E"} ) tree->SetBranchStatus( name, 1);

  MMapVector<Candidate> v( outfilename.c_str(), MMapVector<Candidate>::ReadWriteCreate);

  Candidate cand;
  // have tree use cand as storage
  tree->SetBranchAddress("bkg", &cand.bkg);
  tree->SetBranchAddress("ind1", &cand.ind1);
  tree->SetBranchAddress("ind2", &cand.ind2);
  tree->SetBranchAddress( "m12", &cand.m12);
  tree->SetBranchAddress( "prs1", &cand.prs1);
  tree->SetBranchAddress( "prs2", &cand.prs2);
  tree->SetBranchAddress( "g1E", &cand.g1E);
  tree->SetBranchAddress( "g2E", &cand.g2E);

  Long64_t nEntries = tree->GetEntries();
  v.reserve(nEntries);
  for(Long64_t i=0; i<nEntries; i++){
      tree->GetEntry(i);
      if (!sel(cand.ind1) || !sel(cand.ind2)) continue;
      v.push_back(cand);
  }

  if( file->IsOpen() ) file->Close();
}
Pi0CalibrationFile::Pi0CalibrationFile( const std::string& filename, const std::string& tuplename, const std::string& outfilename ) :
    Pi0CalibrationFile(filename, tuplename, outfilename, [] (unsigned) { return true; } )
{ }
Pi0CalibrationFile::Pi0CalibrationFile( const std::string& filename, const std::string& tuplename, const std::string& outfilename, const std::vector<unsigned int>& indices ) :
    Pi0CalibrationFile(filename, tuplename, outfilename, make_indices_selector(indices) )
{ }


Pi0CalibrationFile::~Pi0CalibrationFile()
{}
