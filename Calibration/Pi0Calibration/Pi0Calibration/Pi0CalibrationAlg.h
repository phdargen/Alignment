/*
 * =====================================================================================
 *
 *       Filename:  Pi0CalibrationAlg.h
 *
 *    Description:  main algorithm for pi0 calibration at LHCb online
 *
 *        Version:  1.0
 *        Created:  11/21/2016 01:47:23 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Zhirui Xu (CNRS-LAPP), Zhirui.Xu@cern.ch
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef CALIBRATION_PI0CALIBRATION_PI0CALIBRATIONALG_H
#define CALIBRATION_PI0CALIBRATION_PI0CALIBRATIONALG_H

#include <string>
#include <map>
#include <vector>
#include <utility>

#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/CaloCellID.h"

#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "Math/SMatrix.h"

#include "Pi0Calibration/Pi0LambdaMap.h"


class Pi0CalibrationAlg : public GaudiAlgorithm {


    public:
	Pi0CalibrationAlg( const std::string& name, ISvcLocator* pSvcLocator);
	virtual ~Pi0CalibrationAlg();

        virtual StatusCode initialize() override;
        virtual StatusCode execute() override;
        virtual StatusCode finalize() override;

    private:
	std::string m_tupleFileName;
        std::string m_tupleName;
        std::string m_outputDir;
	std::string m_lambdaFileName;
	std::string m_saveLambdaFile;
        std::string m_fitMethod;
	std::string m_filetype;

        double m_pdgPi0;

        std::vector<unsigned int > m_indices;
        std::vector<LHCb::CaloCellID> m_cells;

	//std::map<std::string, std::map<unsigned int, Gaudi::XYZPoint > > m_map;
	//std::map<std::string, double> m_cellsize;
  
	std::vector<LHCb::CaloCellID> m_skippedCells;
	//void monitorPlots( std::string name, double centreVal, const std::map<unsigned int, double>& values, bool ownRange=false );

        void saveCells( const std::vector<LHCb::CaloCellID>& cells );
	void saveValues( const std::map<unsigned int, std::pair<double,double> >& values, std::string varname );

};

#endif
