#!/usr/bin/env python

import os, sys, re, math

std_AligWork_dir = '/group/online/AligWork/'
std_alignment_dir = '/group/online/alignment/'
std_new_alignment_dir = 'my_alignments_trendPlots/'
std_references = os.path.expandvars('$ALIGNMENTMONITORINGROOT/files/ConstantsReferences.txt')

##########################
###   Options parser   ###
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description ="Macro to make trend plots alignments")
    parser.add_argument('-a','--alignables', help='eg. VeloLeft.Tx Module01.Rz, for having all 6 dof for an alignable: VeloLeft.* Also regex like Module[0-9]{2}.Tx can be used for the first part (not for the dof) and will be matched to available alignables for the activity', default= ['VeloLeft.Tx', 'VeloLeft.Ty'],nargs='+')
    parser.add_argument('-r','--runs', help='run numbers, default is all the availables', nargs=2, type=int,default=[0, 1e20])
    parser.add_argument('--activity', help='choose between Velo, Tracker, Muon; default is Velo', choices = ['Velo', 'Tracker', 'Muon'] ,default= 'Velo')
    parser.add_argument('-o','--outName',help='output file name without extension, make both pdf and directory', default='trendPlots')
    parser.add_argument('-s', '--samePlot', help='Plot all the alignables in the same plot', action='store_true')
    parser.add_argument('-d', '--drawLegend', help='Draw legend also for plots with only one data-serie', action='store_true')
    parser.add_argument('-f', '--trueUpdate', help='Check the updates comparing constants', action='store_true')
    parser.add_argument('-u', '--diffUpdate', help='Plot difference wrt update', action='store_true')
    parser.add_argument('-n', '--noUpdate', help='Do not plot with empty dots alignments that did not trigger update', action='store_true')
    parser.add_argument('-p', '--projection', help='Make histogram with projection instead of trend', action='store_true')
    parser.add_argument('-l', '--labelAU', help='Use label in AU as x axis', action='store_true')
    parser.add_argument('-y', '--freeY', help='Leave Y axis range free, implies -l', action='store_true')
    parser.add_argument('--rangeY', help='Set range Y axis', nargs=2, type=float)
    parser.add_argument('-c', '--canvasSize', help='Canvas dimensions in pixels, default is 900, 500', nargs=2, type=int, default=[900, 500])
    parser.add_argument('--legPosition', help='Position of the legend, default is 0.8 0.8 .99 .99', nargs=4, type=float, default=[0.8, 0.8, .99, .99])
    parser.add_argument('--legTextSize', help='Size of the text in the legend eg. 0.05', type=float)
    parser.add_argument('--legTransparent', help='The background of the legend is transparent', action='store_true')
    parser.add_argument('--labels', help='They will appear in the label eg. VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation', nargs='+')
    parser.add_argument('--AligWork_dir', help='folder with alignments', default = std_AligWork_dir)
    parser.add_argument('--alignment_dir', help='folder with alignments updated', default = std_alignment_dir)
    parser.add_argument('--references', help='file with references file', default = std_references)
    parser.add_argument('--stickers', help="python dictionary with stickers souronded by \", es. \"{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25): '05/07/2015 - 23/11/2015'}\" ")
    parser.add_argument('-x', '--fromAlignlog', help='Parse alignlog instead of xmls, it plots also erros, N.B. In the alignlo the name of the alignables are different than in the xmls', action='store_true')
    parser.add_argument('--xLabel', help='x-axis label')
    parser.add_argument('--yLabel', help='y-axis label')
    args = parser.parse_args()
    if args.freeY: args.labelAU = True
    args.labels = dict([i.split(':') for i in args.labels])  if args.labels else {}
    exec 'args.stickers = {0}'.format(args.stickers)
        
##########################

import ROOT as r
from AlignmentMonitoring.MultiPlot import MultiPlot

import os, sys, re, shutil, pickle
from GaudiPython import gbl
AlMon = gbl.Alignment.AlignmentMonitoring

from AlignmentMonitoring.LHCbStyle import lhcbStyle # process macro that sets LHCb style
lhcbStyle.SetTitleOffset(0.8,"Y")

r.gROOT.SetBatch(True)

def getListRuns(activity, AligWork_dir = std_AligWork_dir):
    runs = []
    for directory in os.listdir(os.path.join(AligWork_dir, activity)): 
        match = re.findall('^run([0-9]+)$', directory)
        if match: runs.append(int(match[0]))
    return sorted(runs)

    
def getRunFromXml(xml_file):
    text = open(xml_file).read()
    try:
        run = re.findall('<!-- Version:: run([0-9]*) -->', text)[0]
        return int(run)
    except IndexError:
        return None

    
def getListRunsUpdated(activity, alignment_dir = std_alignment_dir):
    runs = []
    if activity == 'Tracker': activity = 'TT'   #CHANGE THE INPUT HERE
    directory = os.path.join(alignment_dir, '{0}/{0}Global'.format(activity))
    for ff in os.listdir(directory):
        run = getRunFromXml(os.path.join(directory, ff))
        if run: runs.append(run)
    return sorted(runs)


def std_vector(list, kind = 'std::string'):
    vec = gbl.std.vector(kind)()
    for i in list:
        vec.push_back(i)
    return vec

def getListRunsTrueUpdate(runs, activity, alignment_dir = std_alignment_dir, AligWork_dir = std_AligWork_dir ):

    from computeTrueUpdate import isUpdate

    if activity == 'Velo':
        detectors = {'Velo' : ['VeloGlobal', 'VeloModules']}
    elif activity == 'Tracker':
        detectors = {'TT' : ['TTGlobal', 'TTModules'],
                    'IT' : ['ITGlobal', 'ITModules'],
                    'OT' : ['OTGlobal', 'OTModules'],}        
        # detectors = {'IT' : ['ITGlobal', 'ITModules']} #CHANGE THE INPUT HERE
    elif activity == 'Muon':
        detectors = {'Muon' : ['MuonGlobal', 'MuonModules']}
    
    updatesRuns = [runs[0]]

    xml_path = os.path.join(AligWork_dir,'{activity}/run{run}/xml/{detector}/{part}.xml')
    
    updates = []
    n_update = 0
    
    for run in runs:
        cc = AlMon.CompareConstants()
        cc.Compare(std_vector([xml_path.format(activity=activity,detector = detector, part=part, run = updatesRuns[-1]) for detector in detectors for part in detectors[detector]]),
                   std_vector([xml_path.format(activity=activity,detector = detector, part=part, run = run) for detector in detectors for part in detectors[detector]]))

        flag, alignables_update = isUpdate(cc, activity)
        
        if flag:
            updatesRuns.append(run)
            updates.append((run,alignables_update))
            n_update = n_update + 1

    return sorted(updatesRuns)

    
def getLimits(dof, alignable, references = std_references):
    for line in open(references).readlines():
        if line[0] != '#':
            try:
                regex, line, maxim = line.split()
                if re.match(regex, '{alignable}.{dof}'.format(**locals())):
                    return (float(line), 1.5*float(maxim))
            except ValueError:
                pass
    raise IOError('No reference for {alignable} {dof}'.format(**locals()))


def readXML(inFile):
    '''
    Read XML file and return dictionary with {name: align_params}
    where align_params is a list [Tx, Ty, Tz, Rx, Ry, Rz]
    '''
    import xml.etree.ElementTree
    text = open(inFile).read()
    if '<DDDB>' not in text:
        text = '<?xml version="1.0" encoding="ISO-8859-1"?><!DOCTYPE DDDB SYSTEM "conddb:/DTD/structure.dtd"><DDDB>\n'+text+'\n</DDDB>'
    root = xml.etree.ElementTree.fromstring(text)
    alignParams = {}
    for alignable in root:
        name = alignable.attrib['name']
        for vec in alignable:
            if vec.attrib['name'] == 'dPosXYZ':
                [Tx, Ty, Tz] = [float(i) for i in vec.text.split()]
            elif vec.attrib['name'] == 'dRotXYZ':
                [Rx, Ry, Rz] = [float(i) for i in vec.text.split()] 

        alignParams[name] = [Tx, Ty, Tz, Rx, Ry, Rz]
    return alignParams


from AlignmentOutputParser.AlignOutput import *

def readAlignlog(inFile):
    '''
    Read alignlog file and return dictionary with {name: align_params}
    where align_params is a ntuple [(Tx, eTx), (Ty, eTy), (Tz, eTz), (Rx, eRx), (Ry, eRy), (Rz, eRz)]
    N.B. here the name of the alignables are different than in the XMLs
    '''
    xmlNames = {
        'Velo/VeloLeft' : 'VeloLeft',
        'Velo/VeloRight' : 'VeloRight',
        'IT/Station1/ASideBox' : 'ITT1ASideBox',
        'IT/Station1/CSideBox' : 'ITT1CSideBox',
        'IT/Station1/TopBox' : 'ITT1TopBox',
        'IT/Station1/BottomBox' : 'ITT1BottomBox',
        'Muon/M1/M1ASide' : 'M1ASide',
        'Muon/M2/M2ASide' : 'M2ASide',
        'Muon/M3/M3ASide' : 'M3ASide',
        'Muon/M4/M4ASide' : 'M4ASide',
        'Muon/M5/M5ASide' : 'M5ASide',
        'Muon/M1/M1CSide' : 'M1CSide',
        'Muon/M2/M2CSide' : 'M2CSide',
        'Muon/M3/M3CSide' : 'M3CSide',
        'Muon/M4/M4CSide' : 'M4CSide',
        'Muon/M5/M5CSide' : 'M5CSide',
        }
    aout = AlignOutput(inFile)
    aout.Parse()
    dofs = ['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz']
    alignParams = {}
    for alignable in aout.AlignIterations[0].Alignables.keys():
        dict_dofs = {i.Name: (i.Cur+i.Delta, i.DeltaErr) for i in aout.AlignIterations[-1].Alignables[alignable].AlignmentDOFs}
        alignParams[xmlNames.get(alignable, alignable)] = [dict_dofs.get(i, (0, 0)) for i in dofs]
        
    return alignParams


def getAllConstants(runs, activity, AligWork_dir = std_AligWork_dir, fromAlignlog=False):
    '''
    return list of pairs (runNumber, all_aligConstants)
    '''
    all_constants = []
    if fromAlignlog:
        for run in runs:
            alignlog = os.path.join(AligWork_dir, '{activity}/run{run}/alignlog.txt'.format(**locals()))
            if not os.path.exists(alignlog): continue
            dictConstants = readAlignlog(alignlog)
            all_constants.append((run, dictConstants))   
    else:
        if activity == 'Velo':
            detectors = ['Velo']
        elif activity == 'Tracker':
            detectors = ['TT', 'IT', 'OT']
        elif activity == 'Muon':
            detectors = ['Muon']
        for run in runs:
            dictConstants = {}    
            for detector in detectors:
                directory = os.path.join(AligWork_dir, '{activity}/run{run}/xml/{detector}'.format(**locals()))
                for xml_file in os.listdir(directory):
                    dictConstants.update(readXML(os.path.join(directory, xml_file)))
            all_constants.append((run, dictConstants))
    return sorted(all_constants, key=lambda x: x[0])
    


def getConstants(dof, alignable, all_constants):
    '''
    given all_constatns produced with previous function
    return list of pairs (runNumber, constant)
    if all_constatns was from alignlog and thus contains also the errors
    constant is a pair (value, error)
    '''
    dofs = ['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz']
    try:
        return [(run, dictConstants[alignable][dofs.index(dof)]) for run, dictConstants in all_constants]
    except KeyError as e:
        raise KeyError('Cannot find alignable "{0}", are you sure you selected the correct activity?'.format(e.message))


def subtractMean (values):
    '''
    Subtract to every value the mean of the serie
    '''
    runs, constErrors = [i[0] for i in values],  [i[1] for i in values]
    try: # len(constErrors[0]) == 2:
        constants, errors = [i[0] for i in constErrors],  [i[1] for i in constErrors]
        mean_const = float(sum(constants))/len(constants)
        return [(run, (const-mean_const, error)) for run, const, error in zip(runs, constants, errors)]
    except TypeError:
        constants = constErrors
        mean_const = float(sum(constants))/len(constants)
        return [(run, const-mean_const) for run, const in zip(runs, constants)]



def diffWRTUpdate(values, runsUpdated):
    valuesUsed = [[i,j] for i,j in values]
    currentValue = valuesUsed[0]
    for i in range(len(valuesUsed)):
        tmp = valuesUsed[i][:]    #[run,value alignable]
        valuesUsed[i][1] = currentValue[1]  #value of the alignables
        try:
            if valuesUsed[i][0] in runsUpdated: currentValue = tmp   
        except IndexError:
            pass
    try:
        return [(i[0], i[1]-j[1]) for i, j in zip(values, valuesUsed)]
    except TypeError: # values are couple (value, error)
        return [(i[0], (i[1][0]-j[1][0], math.sqrt(i[1][1]**2 + j[1][1]**2))) for i, j in zip(values, valuesUsed)]


def makeHisto(name, values, title=''):
    '''
    value is a list of pairs (runNumber, constant)
    '''
    x_values = [i[0] for i in values]
    y_values = [i[1] for i in values]
    # y_errors = [1e-20 for i in y_values]
    histo = r.TH1D(name+'_',title,len(x_values),1,len(x_values)+1)
    for nBin, (x_, y_) in enumerate(zip(x_values, y_values),1):
        if y_ != None:
            r.gStyle.SetErrorX(0.)
            try: # len(y_) == 2:
                histo.Fill(nBin, y_[0])
                histo.SetBinError(nBin, y_[1])
            except TypeError:
                histo.Fill(nBin, y_)
                histo.SetBinError(nBin, 1e-20)
    return histo.Clone(name)


def makeHistoProjection(name, values, title = None, range = [None, None], nBins = None):
    '''
    value is a list of pairs (runNumber, constant)
    '''

    values = [i[1] for i in values]

    #if the value is None put it to 999
    mylist = values
    newlist = []
    for item in mylist:
        if item is None:
            item = 999
        newlist.append(item)
    values = newlist

    if range[0] == None: range[0] = min(values)
    if range[1] == None: range[1] = max(values)+1
    if nBins == None: nBins = int(4*math.sqrt(len(values)))
    if title == None: title = name
    histo = r.TH1D(name+'_tmp', title, nBins, *range)
    for i in values:
        histo.Fill(i)
    return histo.Clone(name)


def addXLabels(mp, maxim, runs):
    text = r.TText()
    text.SetTextAlign(32)
    text.SetTextAngle(-80)
    text.SetTextSize(0.02)
    axis = mp.hs.GetXaxis()
    for nBin,fillID in enumerate(runs, 1):
        xlow = axis.GetBinLowEdge(nBin)
        xup  = axis.GetBinUpEdge(nBin)
        xmid = axis.GetBinCenter(nBin)
        text.DrawText(xmid,-1.2*maxim,str(int(fillID)))

               

def drawPlot(allConstants, runsUpdated, aligndofs, diffUpdate = False, noUpdate = False, labelAU = False, freeY = False, rangeY = None, isProjection = False, references = std_references, onlyRuns2show=None, drawLegend41=False, legPosition=(0.8, 0.8, .99, .99), legTransparent=False, legTextSize=None, labels={}, stickers={}, xLabel=None, yLabel=None, *args, **kwargs):
    '''
    aligndof is a list of alignables and dof eg [VeloLeft.Tx, ]
    '''

    if not labelAU and not isProjection:
        r.gStyle.SetLabelSize(0,"x")
        r.gStyle.SetTitleOffset(1.1,"X")
    else:
        r.gStyle.SetTitleOffset(1,"X")

    if isinstance(aligndofs, basestring):
        aligndofs = [aligndofs]


    alignable, dof = aligndofs[0].split('.')
    line, maxim = getLimits(dof, alignable, references)
    
    if 'T' in dof:
        if maxim < 1e-1:
            mult = 1000
            unit = '#mum'
        else:
            mult = 1
            unit = 'mm'
    elif 'R' in dof:
        if maxim < 1e-3:
            mult = 1000000
            unit = '#murad'
        elif maxim < 1e-1:
            mult = 1000
            unit = 'mrad'
        else:
            mult = 1
            unit = 'rad'
    line, maxim = line*mult, maxim*mult

    lines = [0,-1*line,line] if diffUpdate else [0]
    if isProjection:
        vlines, hlines = lines, []
        vlines_styles, hlines_styles = {i : (1 if i == 0 else 7) for i in lines}, {}
        if not rangeY: rangeY = (None, None)
        if not xLabel: xLabel = ('Variation' if diffUpdate else 'Value') + ' [{0}]'.format(unit)
        yLabel = 'Entries'
    else:
        vlines, hlines = [], lines
        vlines_styles, hlines_styles = {},  {i : (1 if i == 0 else 7) for i in lines}
        if not rangeY: rangeY = (None, None) if freeY else (-1*maxim, maxim)
        if not xLabel: xLabel = 'Alignment number [a.u.]' if labelAU else 'Run number'
        if not yLabel: yLabel = ('Variation' if diffUpdate else 'Value') + ' [{0}]'.format(unit)
    

    mp = MultiPlot('TrendPlot', title = 'Trend plot {alignable} {dof};{xLabel};{yLabel}'.format(**locals()),
    kind='h', legMarker='p',
    hlines=hlines, vlines = vlines,
    vlines_styles = vlines_styles, hlines_styles = hlines_styles,
    rangeY = rangeY,
    drawLegend= len(aligndofs) != 1 or drawLegend41,
    legPosition=legPosition, legTransparent=legTransparent,
    legTextSize=legTextSize, stickers=stickers,
    )

    for style, aligndof in enumerate(aligndofs, 1):
        alignable, dof = aligndof.split('.')
        if diffUpdate:
             values = diffWRTUpdate(getConstants(dof, alignable, allConstants), runsUpdated)
        else:
            values = subtractMean(getConstants(dof, alignable, allConstants))
        try: # len(values[0][1]) == 2:
            values = [(i, (j[0]*mult, j[1]*mult)) for i,j in values]
        except TypeError:
            values = [(i,j*mult) for i,j in values]
        if onlyRuns2show != None:
            new_values = []
            for value in values:
                if value[0] in onlyRuns2show:
                    new_values.append(value)
            values = new_values
        valuesUpdated = [[i,j] for i,j in values]
        if not noUpdate:
            for value in valuesUpdated:
                if value[0] not in runsUpdated: value[1] = None
        if isProjection:
            histo = makeHistoProjection(dof, values, range=(-1*maxim, maxim))
            label = labels.get('{0}.{1}'.format(alignable, dof), '{0} {1}'.format(alignable, dof))
            mp.Add(histo, label, style=style)
            mp.Draw()
        else:
            histo = makeHisto(dof, values)
            histoUpdated = makeHisto(dof, valuesUpdated)
            label = labels.get('{0}.{1}'.format(alignable, dof), '{0} {1}'.format(alignable, dof))
            if all(v[1] is None for v in valuesUpdated): # Any update present (e.g. for Muon)
                label = labels.get('{0}.{1}'.format(alignable, dof), '{0} {1}'.format(alignable, dof))
                mp.Add(histo, label, style=-1*style, lineStyle=1)
            else:
                mp.Add(histo, style=-1*style, lineStyle=1)
                mp.Add(histoUpdated, label, style=style, lineStyle=1)
            mp.Draw()
            if not labelAU:
                addXLabels(mp, maxim, [i[0] for i in values])

                
    return mp


if __name__ == '__main__':
        
    runs = getListRuns(args.activity, args.AligWork_dir)
    runs = [i for i in runs if i >= args.runs[0] and i <= args.runs[1]] 

    if args.trueUpdate:
        runsUpdated = getListRunsTrueUpdate(runs, args.activity, args.alignment_dir, args.AligWork_dir)
    else:
        runsUpdated = getListRunsUpdated(args.activity, args.alignment_dir)
        runsUpdated = [i for i in runsUpdated if i in runs] 
        

    outFile_name = args.outName+'.pdf'
    outDir = args.outName
    if not os.path.exists(outDir): os.mkdir(outDir)

    allConstants = getAllConstants(runs, args.activity, args.AligWork_dir, fromAlignlog = args.fromAlignlog)

    # Accept regex in alignables
    allAlignables = allConstants[0][1].keys()
    alignables = []
    for alignableDof in args.alignables:
        alignable_regex, dof = '.'.join(alignableDof.split('.')[:-1]), alignableDof.split('.')[-1]
        if alignable_regex[-1] == '\\':
            alignable_regex = alignable_regex[:-1]
        for alignable in allAlignables:
            if re.match('^'+alignable_regex+'$', alignable):
                alignables.append('.'.join([alignable, dof]))

    
    c = r.TCanvas('c', 'c', *args.canvasSize)
    c.Print(outFile_name+'[')

   
    if args.samePlot:
        mp=drawPlot(allConstants, runsUpdated, alignables, isProjection=args.projection, drawLegend41=args.drawLegend, **vars(args))
        c.Print(outFile_name)
        c.Print(os.path.join(outDir,'{0}_{1}.pdf'.format(*alignables[0].replace('/','_').split('.'))))
        c.Print(os.path.join(outDir,'{0}_{1}.C'.format(*alignables[0].replace('/','_').split('.'))))

    else:
        for align in alignables:
            alignable, dof = align.split('.')
            dofs = ['Tx', 'Ty', 'Tz', 'Rx', 'Ry', 'Rz'] if dof == '*' else [dof]

            for dof in dofs:
                mp=drawPlot(allConstants, runsUpdated, '{0}.{1}'.format(alignable, dof), isProjection=args.projection, drawLegend41=args.drawLegend, **vars(args))
                c.Print(outFile_name)
                c.Print(os.path.join(outDir,'{0}_{1}.pdf'.format(alignable.replace('/','_'), dof)))
                c.Print(os.path.join(outDir,'{0}_{1}.C'.format(alignable.replace('/','_'), dof)))

    c.Print(outFile_name+']')

