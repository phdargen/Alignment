#!/usr/bin/env bash

rm -r publicityPlots
mkdir tmp

# Velo Tx Ty
$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsul  -a VeloLeft.Tx VeloLeft.Ty \
		-r 172933 500000 \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 06/06/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_stability_Txy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl  -a VeloLeft.Tx VeloLeft.Ty \
		-r 172933 500000 \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 06/06/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_trend_Txy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsul  -a VeloLeft.Tx VeloLeft.Ty \
		-r 172933 500000 \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 06/06/2016'}" \
		-o tmp/Velo_stability_Txy_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl  -a VeloLeft.Tx VeloLeft.Ty \
		-r 172933 500000 \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 06/06/2016'}" \
		-o tmp/Velo_trend_Txy_fd

# Muon Tx Ty

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1ASide.Tx  M2ASide.Tx M3ASide.Tx M4ASide.Tx M5ASide.Tx \
		-r 174362 500000  --yLabel "#DeltaX Variation [mm]"\
		--labels M1ASide.Tx:M1 M2ASide.Tx:M2 M3ASide.Tx:M3 M4ASide.Tx:M4 M5ASide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonA_stability_Tx

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1ASide.Ty  M2ASide.Ty M3ASide.Ty M4ASide.Ty M5ASide.Ty \
		-r 174362 500000  --yLabel "#DeltaY Variation [mm]"\
		--labels M1ASide.Ty:M1 M2ASide.Ty:M2 M3ASide.Ty:M3 M4ASide.Ty:M4 M5ASide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonA_stability_Ty

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1CSide.Tx  M2CSide.Tx M3CSide.Tx M4CSide.Tx M5CSide.Tx \
		-r 174362 500000  --yLabel "#DeltaX Variation [mm]"\
		--labels M1CSide.Tx:M1 M2CSide.Tx:M2 M3CSide.Tx:M3 M4CSide.Tx:M4 M5CSide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonC_stability_Tx

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl -a M1CSide.Ty  M2CSide.Ty M3CSide.Ty M4CSide.Ty M5CSide.Ty \
		-r 174362 500000  --yLabel "#DeltaY Variation [mm]"\
		--labels M1CSide.Ty:M1 M2CSide.Ty:M2 M3CSide.Ty:M3 M4CSide.Ty:M4 M5CSide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonC_stability_Ty

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1ASide.Tx  M2ASide.Tx M3ASide.Tx M4ASide.Tx M5ASide.Tx \
		-r 174362 500000  --yLabel "#DeltaX Variation [mm]"\
		--labels M1ASide.Tx:M1 M2ASide.Tx:M2 M3ASide.Tx:M3 M4ASide.Tx:M4 M5ASide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonA_stability_Tx_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1ASide.Ty  M2ASide.Ty M3ASide.Ty M4ASide.Ty M5ASide.Ty \
		-r 174362 500000  --yLabel "#DeltaY Variation [mm]"\
		--labels M1ASide.Ty:M1 M2ASide.Ty:M2 M3ASide.Ty:M3 M4ASide.Ty:M4 M5ASide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon A-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonA_stability_Ty_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1CSide.Tx  M2CSide.Tx M3CSide.Tx M4CSide.Tx M5CSide.Tx \
		-r 174362 500000  --yLabel "#DeltaX Variation [mm]"\
		--labels M1CSide.Tx:M1 M2CSide.Tx:M2 M3CSide.Tx:M3 M4CSide.Tx:M4 M5CSide.Tx:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonC_stability_Tx_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsl -a M1CSide.Ty  M2CSide.Ty M3CSide.Ty M4CSide.Ty M5CSide.Ty \
		-r 174362 500000  --yLabel "#DeltaY Variation [mm]"\
		--labels M1CSide.Ty:M1 M2CSide.Ty:M2 M3CSide.Ty:M3 M4CSide.Ty:M4 M5CSide.Ty:M5 \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.7 0.9 0.9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Muon C-side}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '14/05/2016 - 06/06/2016'}" \
		--activity Muon -o tmp/MuonC_stability_Ty_fd

# Tracker Tx Tz

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsul -a ITT1ASideBox.Tx ITT1CSideBox.Tx ITT1TopBox.Tx ITT1BottomBox.Tx \
		-r 173422 500000 \
		--rangeY -0.2 0.2 --yLabel "#DeltaX Variation [mm]"\
		--labels ITT1ASideBox.Tx:"IT1 ASide" ITT1CSideBox.Tx:"IT1 CSide" ITT1TopBox.Tx:"IT1 Top" ITT1BottomBox.Tx:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '06/05/2016 - 06/06/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		--activity Tracker -o tmp/Tracker_stability_Tx

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsul -a ITT1ASideBox.Tz ITT1CSideBox.Tz ITT1TopBox.Tz ITT1BottomBox.Tz \
		-r 173422 500000 \
		--rangeY -1 1 --yLabel "#DeltaZ Variation [mm]"\
		--labels ITT1ASideBox.Tz:"IT1 ASide" ITT1CSideBox.Tz:"IT1 CSide" ITT1TopBox.Tz:"IT1 Top" ITT1BottomBox.Tz:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '06/05/2016 - 06/06/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		--activity Tracker -o tmp/Tracker_stability_Tz

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsul -a ITT1ASideBox.Tx ITT1CSideBox.Tx ITT1TopBox.Tx ITT1BottomBox.Tx \
		-r 173422 500000 \
		--rangeY -0.2 0.2 --yLabel "#DeltaX Variation [mm]"\
		--labels ITT1ASideBox.Tx:"IT1 ASide" ITT1CSideBox.Tx:"IT1 CSide" ITT1TopBox.Tx:"IT1 Top" ITT1BottomBox.Tx:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '06/05/2016 - 06/06/2016'}" \
		--activity Tracker -o tmp/Tracker_stability_Tx_fd

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -nxsul -a ITT1ASideBox.Tz ITT1CSideBox.Tz ITT1TopBox.Tz ITT1BottomBox.Tz \
		-r 173422 500000 \
		--rangeY -1 1 --yLabel "#DeltaZ Variation [mm]"\
		--labels ITT1ASideBox.Tz:"IT1 ASide" ITT1CSideBox.Tz:"IT1 CSide" ITT1TopBox.Tz:"IT1 Top" ITT1BottomBox.Tz:"IT1 Bottom" \
		--legTransparent --legTextSize 0.05 --legPosition 0.7 0.73 0.9 0.93 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb Tracker}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '06/05/2016 - 06/06/2016'}" \
		--activity Tracker -o tmp/Tracker_stability_Tz_fd


mkdir publicityPlots
cp tmp/*.pdf publicityPlots
rm -r tmp
