#!/usr/bin/env bash

rm -r veloPlots
mkdir tmp

# Velo Tx Ty
$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsul  -a VeloLeft.Tx VeloLeft.Ty \
		-r 172933 500000 \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_stability_Txy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl  -a VeloLeft.Tx VeloLeft.Ty \
		-r 172933 500000 \
		--labels VeloLeft.Tx:x-translation VeloLeft.Ty:y-translation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_trend_Txy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsul  -a VeloLeft.Rx VeloLeft.Ry \
		-r 172933 500000 \
		--labels VeloLeft.Rx:x-rotation VeloLeft.Ry:y-rotation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_stability_Rxy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsl  -a VeloLeft.Rx VeloLeft.Ry \
		-r 172933 500000 \
		--labels VeloLeft.Rx:x-rotation VeloLeft.Ry:y-rotation --legTextSize 0.05 \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_trend_Rxy

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsuld  -a VeloLeft.Tz \
		-r 172933 500000 \
		--labels VeloLeft.Tz:z-translation  \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_stability_Tz

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsld  -a VeloLeft.Tz \
		-r 172933 500000 \
		--labels VeloLeft.Tz:z-translation  \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_trend_Tz

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsuld  -a VeloLeft.Rz \
		-r 172933 500000 \
		--labels VeloLeft.Rz:z-rotation  \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_stability_Rz

$ALIGNMENTMONITORINGROOT/scripts/trendPlots.py -xsld  -a VeloLeft.Rz \
		-r 172933 500000 \
		--labels VeloLeft.Rz:z-rotation  \
		--legTransparent --legTextSize 0.05 --legPosition .65 0.8 .9 .9 \
		--stickers "{(.2,.7,0.45,0.9, 0.05): ['#scale[1.5]{LHCb VELO}', '#scale[1.3]{Preliminary}'], (.65,.2,0.9,0.25,0.05): '23/04/2016 - 23/11/2016',  (.25,.2,0.4,0.25,0.05): 'Empty markers = no update'}" \
		-o tmp/Velo_trend_Rz


mkdir veloPlots
cp tmp/*.pdf veloPlots
rm -r tmp
