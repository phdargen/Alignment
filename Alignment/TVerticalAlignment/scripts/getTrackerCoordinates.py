from Gaudi.Configuration import *

alignname = 'YAlignITDB'

## Import default LHCb application options
from Configurables import CondDBAccessSvc, CondDB, UpdateManagerSvc
CondDB().UseOracle = False #Boolean flag to enable the usage of the CondDB from Oracle servers
CondDB().UseLatestTags = ['2015']
#CondDB().IgnoreHeartBeat= True
#Implement local alignment files
localDb= CondDBAccessSvc(  "myCondLocal",
        ConnectionString = "sqlite_file:/afs/cern.ch/user/z/zhxu/work/TrackYAlignment/database_2015/YAlignITDB.db/LHCBCOND",
              DefaultTAG = "YAlignITDB",
                        )
CondDB().addLayer( localDb )

# Get the event time (for CondDb) from ODIN
from Configurables import GaudiSequencer, LHCbApp

lhcbApp  = LHCbApp()
lhcbApp.DataType = '2015' 
#lhcbApp.Simulation = True
lhcbApp.CondDBtag = "cond-20150602"
lhcbApp.DDDBtag   = "dddb-20150526"

from GaudiPython import * 
ApplicationMgr( OutputLevel = INFO, AppName = 'OT' )
ApplicationMgr().EvtMax = 1

mainSeq = GaudiSequencer( 'MainSeq' )
mainSeq.OutputLevel = INFO
ApplicationMgr().TopAlg.append( mainSeq )

from Configurables import STCoordinates, OTCoordinates

itCoord = STCoordinates("ITCoordinates")
itCoord.DetType = "IT"
itCoord.AlignmentTag = alignname

ttCoord = STCoordinates("TTCoordinates")
ttCoord.DetType = "TT"
ttCoord.AlignmentTag = alignname

otCoord = OTCoordinates("OTCoordinates")
otCoord.AlignmentTag = alignname

GaudiSequencer('MainSeq').Members = [itCoord, ttCoord, otCoord]
#GaudiSequencer('MainSeq').Members = [itCoord]

appMgr = AppMgr()
appMgr.run(1)
