#include "TVerticalAlignment/ITYAlignMagOff.h"
#include <boost/filesystem.hpp>

using namespace std;
using namespace RooFit;
using namespace Alignment::TVerticalAlignment;
class STNames;

ITYAlignMagOff::ITYAlignMagOff(TString filename, TString dbfilename, TString txtfiledir, bool constraint, bool saveplots) :
  m_filename(filename), m_dbfilename(dbfilename), m_outDirectory(txtfiledir), m_constraint(constraint), m_saveplots(saveplots)
{

  m_va = new TVerticalAlignment();
  m_va->initParameter(std::string("IT"));
  m_param = m_va->getParameter(std::string("IT"));
  m_Names = new STNames("IT");

  if(!boost::filesystem::exists(m_outDirectory.Data())) boost::filesystem::create_directories(m_outDirectory.Data());

  boost::filesystem::path s(m_filename.Data());
  m_outputname = "ITYAlign_"+s.filename().string()+"_eff";
  m_outputname.ReplaceAll(".root","");

}


void ITYAlignMagOff::glimpse_data() { m_glimpse = true; }
void ITYAlignMagOff::debug_verbose() { m_verbose = true; }

RooDataSet* ITYAlignMagOff::GetRooDataSetFromTH1(TH1F* histo, TH1F* histo_exp, RooRealVar y, RooRealVar weight,
                                   double y_left_min,  double y_left_max, 
                                   double y_right_min, double y_right_max, 
                                   RooCategory cat_lr, RooCategory cat_eff)
{
  double center;
  RooDataSet* data = new RooDataSet("data","data",RooArgSet(y,weight,cat_lr,cat_eff),WeightVar(weight));
  for(int i(1); i <= histo->GetNbinsX(); i++){
    center = histo->GetBinCenter(i);
    y.setVal(center);
    if(center < 0.)
      cat_lr.setLabel("left");
    else
      cat_lr.setLabel("right");
    
    cat_eff.setLabel("found");
    
    if( (cat_lr == "left" && (center < y_left_min || center > y_left_max)) || 
        (cat_lr == "right" && (center < y_right_min || center > y_right_max))
      ) continue;

    data->add(RooArgSet(y,cat_lr,cat_eff),histo->GetBinContent(i));
    cat_eff.setLabel("not found");
    data->add(RooArgSet(y,cat_lr,cat_eff),histo_exp->GetBinContent(i)-histo->GetBinContent(i));
  }
  
  return data;
}

void ITYAlignMagOff::GetMeanFromHisto(TString SectorName)
{
  //all the sectors with name Sector4 in Top and Bottom boxes are excluded
  if( (SectorName.Contains("Top") || SectorName.Contains("Bottom")) && (SectorName.Contains("Sector4")) ) return;

  if(m_verbose) std::cout << " Sector : " << SectorName << " to go." << std::endl;

  auto m_IT = boost::get<STParam>(m_param);
  double ParTol          = m_IT.tolerance();
  double Scale           = m_IT.scale(); 
  double SigmaConstraint = m_IT.sigmaconstraint();

  int Sectorno( m_Names->GetSectNames( SectorName.Data() )->Sectno );
  if(SectorName.Contains("ASide"))
    Scale += ( Sectorno - 3 )*2.;
  else if(SectorName.Contains("CSide"))
    Scale -= ( Sectorno - 5 )*2.;
  else
    Scale += TMath::Abs( Sectorno - 4 )*3. -6.;
  
  
  TFile* datafile = new TFile(m_filename);
  TString directory(m_IT.directory().first);
  TH1F* _yeff     = (TH1F*)datafile->Get((TString)(directory+SectorName));
  TString directory_exp(m_IT.directory().second);
  TH1F* _yeff_exp = (TH1F*)datafile->Get((TString)(directory_exp+SectorName));
  
  if(m_verbose) cout << SectorName << " read from " << directory+SectorName << " and " << directory_exp+SectorName << endl;

  if(!_yeff || !_yeff_exp) return;


  double Edge, Length;
  if(m_Names->GetNbSensors(SectorName.Data()) == 2)
    Length = 218.15;
  else
    Length = 108.;
  Edge = Length/2.;
 
  //variable for y position
  RooRealVar y("y","y",-150.,150.);
  y.setBins(30);
  y.setRange("left",-Edge-Scale,-Edge+Scale);
  y.setRange("right",Edge-Scale,Edge+Scale);
  y.setRange("full",-Edge-Scale,Edge+Scale);
  //Construct a category with labels and indeces: define a category with explicitly numbered states 
  RooCategory cat_lr("cat_lr","cat_lr");
  cat_lr.defineType("left",-1) ;
  cat_lr.defineType("center",0) ;
  cat_lr.defineType("right",1) ;
  RooCategory cat_eff("cat_eff","cat_eff");
  cat_eff.defineType("found",1) ;
  cat_eff.defineType("not found",0) ;
  //weight variable in dataset
  RooRealVar weight("weight","weight",0,100000000000000);
  RooRealVar N_l("N_l","N",0.05,0.85);
  RooRealVar N_r("N_r","N",0.05,0.85);

  RooDataSet* yeff_data = 0;
  yeff_data = GetRooDataSetFromTH1(_yeff,_yeff_exp,y,weight,y.getMin("left"),0., 0.,y.getMax("right"),cat_lr,cat_eff);

  if( datafile->IsOpen() ) datafile->Close();

  //define the fitting models
  RooRealVar center("center","center",0.,-5.,5.,"mm");
  RooRealVar length("length","Length",100.,100.,250.,"mm");
  RooFormulaVar mu_l("mu_l","center-length/2.",RooArgSet(center,length));
  RooFormulaVar mu_r("mu_r","center+length/2.",RooArgSet(center,length));
  RooRealVar sigma_l("sigma_l","#sigma_{l}",2.,0.5,4.,"mm");
  RooRealVar rsigma_l("rsigma_l","Ratio #sigma_{l}",20.,1.2,100.,"mm");
  RooRealVar sigma_r("sigma_r","#sigma_{r}",2.,0.5,4.,"mm");
  RooRealVar rsigma_r("rsigma_r","Ratio #sigma_{r}",20.,1.2,100.,"mm");
  RooFormulaVar y_l_1("y_l_1","(y-mu_l)/(TMath::Sqrt(2)*sigma_l)",RooArgSet(y,mu_l,sigma_l));
  RooFormulaVar y_l_2("y_l_2","(y-mu_l)/(TMath::Sqrt(2)*sigma_l*rsigma_l)",RooArgSet(y,mu_l,sigma_l,rsigma_l));
  RooFormulaVar y_r_1("y_r_1","(y-mu_r)/(TMath::Sqrt(2)*sigma_r)",RooArgSet(y,mu_r,sigma_r));
  RooFormulaVar y_r_2("y_r_2","(y-mu_r)/(TMath::Sqrt(2)*sigma_r*rsigma_r)",RooArgSet(y,mu_r,sigma_r,rsigma_r));
  RooRealVar sigslope_l("sigslope_l","Sig Slope",0.,-0.018,0.018,"");
  RooRealVar bkgslope_l("bkgslope_l","Bkg Slope",0.,-0.015,0.015,"");
  RooRealVar sigfrac_l("sigfrac_l","Sig Frac",0.5,0.01,1.,"");
  RooRealVar sig12frac_l("sig12frac_l","Sig Frac",0.5,0.1,1.,"");
  RooRealVar sigslope_r("sigslope_r","Sig Slope",0.,-0.018,0.018,"");
  RooRealVar bkgslope_r("bkgslope_r","Bkg Slope",0.,-0.015,0.015,"");
  RooRealVar sigfrac_r("sigfrac_r","Sig Frac",0.5,0.01,1.,"");
  RooRealVar sig12frac_r("sig12frac_r","Sig Frac",0.5,0.1,1.,"");
  
  N_l.setVal(0.5);
  N_r.setVal(0.5);
  center.setVal(0.);
  center.setRange(0.-ParTol,0.+ParTol);
  length.setVal(Length);
  length.setRange(Length-ParTol,Length+ParTol);
  sigma_l.setVal(1.);
  rsigma_l.setVal(20.);
  sigslope_l.setVal(0.0001);
  bkgslope_l.setVal(0.0);
  sigfrac_l.setVal(0.99);
  sigma_r.setVal(1.);
  rsigma_r.setVal(20.);
  sigslope_r.setVal(0.0001);
  bkgslope_r.setVal(0.0);
  sigfrac_r.setVal(0.99);
  
  sig12frac_l.setVal(0.6);
  sig12frac_r.setVal(0.6);
  if((SectorName.Contains("Bottom") || SectorName.Contains("Top")) && (SectorName.Contains("Sector3") || SectorName.Contains("Sector5"))){
    bkgslope_l.setConstant(false);
    bkgslope_r.setConstant(false);
    bkgslope_l.setVal(0.001);
    bkgslope_r.setVal(0.001);
  }else{
    bkgslope_l.setConstant();
    bkgslope_r.setConstant();
  }
  
  TString formula_l("N_l*(sigfrac_l*(1.+sigslope_l*(y-(center-length/2.)))*(0.5+0.5*sig12frac_l*TMath::Erf(y_l_1)+0.5*(1.-sig12frac_l)*TMath::Erf(y_l_2))+(1.-sigfrac_l)*(1.+bkgslope_l*(y-(center-length/2.))))*(y<0.)");
  TString formula_r("N_r*(sigfrac_r*(1.+sigslope_r*(y-(center+length/2.)))*(0.5+0.5*sig12frac_r*TMath::Erf(-y_r_1)+0.5*(1.-sig12frac_r)*TMath::Erf(-y_r_2))+(1.-sigfrac_r)*(1.+bkgslope_r*(y-(center+length/2.))))*(y>0.)");
  TString formula_lr(formula_l);
  formula_lr += "+";
  formula_lr += formula_r;
  
  RooArgSet arg_commun(y,center,length);
  RooArgSet arg_l(y_l_1,y_l_2,sigma_l,rsigma_l,sig12frac_l,sigfrac_l,sigslope_l,bkgslope_l,N_l);
  RooArgSet arg_r(y_r_1,y_r_2,sigma_r,rsigma_r,sig12frac_r,sigfrac_r,sigslope_r,bkgslope_r,N_r);
  arg_l.add(arg_commun);
  arg_r.add(arg_commun);
  RooArgSet arg_lr(arg_l);
  arg_lr.add(arg_r);
  
  RooFormulaVar* _model_eff_l = new RooFormulaVar("_model_eff_l",formula_l,arg_l);
  RooEfficiency*  model_eff_l = new RooEfficiency( "model_eff_l","model_eff_l",*_model_eff_l,cat_eff,"found");
  RooFormulaVar* _model_eff_r = new RooFormulaVar("_model_eff_r",formula_r,arg_r);
  RooEfficiency*  model_eff_r = new RooEfficiency( "model_eff_r","model_eff_r",*_model_eff_r,cat_eff,"found");
  
  RooFormulaVar* _model_eff   = new RooFormulaVar("_model_eff",formula_lr,arg_lr);
  RooEfficiency*  model_eff   = new RooEfficiency("model_eff","model_eff",*_model_eff,cat_eff,"found");

  RooDataSet* yeff_l = (RooDataSet*)yeff_data->reduce("cat_lr == cat_lr::left");
  RooDataSet* yeff_r = (RooDataSet*)yeff_data->reduce("cat_lr == cat_lr::right");

  RooFitResult* fitresults;

  if(!m_glimpse){ 

    //sectors with name Sector3 and Sector5 in Top and Bottom boxes are fitted only one side
    if(SectorName.Contains("Top")&&(SectorName.Contains("Sector3")||SectorName.Contains("Sector5"))){
     
      length.setConstant();
      fitresults = model_eff_l->fitTo(*yeff_l, Minos(true), Range("left"), PrintLevel(m_verbose? 1:-1), 
                                      /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4),
                                      Strategy(1), Save(1));
      if(m_verbose){
        cout << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        cout << fitresults->status() << endl;
      }

      if(fitresults->status() == 0 || fitresults->status() == 6)
        m_YPosFitFile << SectorName << " " << center.getVal() << " " << center.getError() << endl;
      
    }else if(SectorName.Contains("Bottom")&&(SectorName.Contains("Sector3")||SectorName.Contains("Sector5"))){
    
      length.setConstant();
      fitresults = model_eff_r->fitTo(*yeff_r, Minos(true), Range("right"), PrintLevel(m_verbose? 1:-1), 
                                      /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4),
                                      Strategy(1), Save(1));
      
      if(m_verbose){
        cout << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        cout << fitresults->status() << endl;
      }

      if(fitresults->status() == 0 || fitresults->status() == 6)
        m_YPosFitFile << SectorName << " " << center.getVal() << " " << center.getError() << endl;
    
    }else{
      center.setConstant();
      model_eff_l->fitTo(*yeff_l, Minos(true), Range("left"), PrintLevel(m_verbose? 1:-1), 
                         /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4), Strategy(1));
      
      model_eff_r->fitTo(*yeff_r, Minos(true), Range("right"), PrintLevel(m_verbose? 1:-1), 
                         /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4), Strategy(1));
      center.setConstant(false);
      length.setVal(Length);
      if(m_constraint){
        RooGaussian* fconstext = new RooGaussian("fconstext","fconstext",length,RooConst(Length),RooConst(SigmaConstraint)) ;
    
        fitresults = model_eff->fitTo(*yeff_data, Minos(true), Range("left,right"), PrintLevel(m_verbose? 1:-1), 
                                      /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4), 
                                      Strategy(1), Save(1), ExternalConstraints(*fconstext));
      }else{
        fitresults = model_eff->fitTo(*yeff_data, Minos(true), Range("left,right"), PrintLevel(m_verbose? 1:-1), 
                                      /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4),
                                      Strategy(1), Save(1));      
      }
      
      if(m_verbose){
        cout << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        cout << SectorName << " " << length.getVal() << " " << length.getError() << endl;
        cout << fitresults->status() << endl;
      }

      if(fitresults->status() == 0 || fitresults->status() == 6){
        m_YPosFitFile   << SectorName << " " << center.getVal() << " " << center.getError() << endl;
        m_LengthFitFile << SectorName << " " << length.getVal() << " " << length.getError() << endl;
      }

    }
  }

  TCanvas* canvas = new TCanvas("canvas","canvas",1920,1080);
  RooPlot* frame = y.frame(Range("full"),Bins(100));
  frame->SetTitle(SectorName);
  yeff_data->plotOn(frame,Efficiency(cat_eff));
  if(!m_glimpse){
    if( !(SectorName.Contains("Bottom")&&(SectorName.Contains("Sector3")||SectorName.Contains("Sector5"))) )
      _model_eff_l->plotOn(frame,Range("left"),Normalization(_model_eff_l->createIntegral(y,"left")->getVal(),RooAbsReal::NumEvent));
    if( !(SectorName.Contains("Top")&&(SectorName.Contains("Sector3")||SectorName.Contains("Sector5"))) )
      _model_eff_r->plotOn(frame,Range("right"),Normalization(_model_eff_r->createIntegral(y,"right")->getVal(),RooAbsReal::NumEvent));
  }
  frame->Draw();
  frame->GetXaxis()->SetTitle("Local Y [mm]");
  frame->GetYaxis()->SetTitle("Efficiency []");
      
  if(m_saveplots){
    boost::filesystem::path outpath(m_outDirectory);
    boost::filesystem::path plotpath("plots/"+m_outputname);
    if(!boost::filesystem::exists(outpath/plotpath)) boost::filesystem::create_directories(outpath/plotpath);
    boost::filesystem::path plotname(SectorName+"_edge_eff.eps");
    canvas->SaveAs( (outpath/plotpath/plotname).c_str() );
  }
  canvas->Close();
}

void ITYAlignMagOff::FitfromROOTFile()
{
  STNames::Vector* sectors(m_Names->GetSectors());
  STNames::Vector::iterator It, Begin( sectors->begin() ), End( sectors->end() );
  for (It = Begin; It != End; It++){
    GetMeanFromHisto( TString((*It).c_str()) );
  }
  sectors->clear(); 
}

void ITYAlignMagOff::fit_efficiency()
{
  if(!m_verbose){
    RooMsgService::instance().setSilentMode(true);
    RooMsgService::instance().setStreamStatus(1,false);
    RooFit::Verbose(false);
  }
  TString Titlename("Local Y Position");
  gROOT->SetStyle("Plain"); 
  gStyle->SetOptStat(0);
  gStyle->SetTitleX(0.5);
  gStyle->SetTitleAlign(23); 
  gStyle->SetTitleBorderSize(0);
  gStyle->SetPaintTextFormat("5.1f");
  gStyle->SetStatFontSize(0.15);
  gStyle->SetTitleFontSize(0.07);

  boost::filesystem::path outpath(m_outDirectory);
  boost::filesystem::path fitpath("fit");
  if(!boost::filesystem::exists(outpath/fitpath)) boost::filesystem::create_directories(outpath/fitpath);
 
  boost::filesystem::path yposfilename(m_outputname+".txt");
  boost::filesystem::path yposfile = outpath/fitpath/yposfilename;  
  m_YPosFitFile.open( yposfile.c_str() );

  boost::filesystem::path lengthfilename(m_outputname+"_length.txt");
  boost::filesystem::path lengthfile = outpath/fitpath/lengthfilename;
  m_LengthFitFile.open( lengthfile.c_str() );
  
  FitfromROOTFile();

  m_YPosFitFile.close();
  m_LengthFitFile.close();
}

void ITYAlignMagOff::plots(){
  double Length_Long(218.15);
  double Length_Short(108.);
  double Scale(3.);

  RooMsgService::instance().setSilentMode(true);
  RooMsgService::instance().setStreamStatus(1,false);

  boost::filesystem::path outpath(m_outDirectory);
  boost::filesystem::path plotpath("plots/"+m_outputname);
  if(!boost::filesystem::exists(outpath/plotpath)) boost::filesystem::create_directories(outpath/plotpath);
  
  TString Titlename_YPos("IT Y Position");
  TString Titlename_Length("Active Length");
  
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetTitleX(0.5);
  gStyle->SetTitleAlign(23);
  gStyle->SetTitleBorderSize(0);
  gStyle->SetPaintTextFormat("5.1f");
  gStyle->SetStatFormat("5.5f");
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetPadTickY(1);
  gStyle->SetPadTickX(1);
  
  unsigned int nColors=52;
  Int_t MyPalette[52];
  Double_t s[3] = {0.00, 0.50, 1.00};
  Double_t b[3] = {0.80, 1.00, 0.00};
  Double_t g[3] = {0.00, 1.00, 0.00};
  Double_t r[3] = {0.00, 1.00, 0.80};
  Int_t FI = TColor::CreateGradientColorTable(3, s, r, g, b, nColors);
  for (unsigned int k(0); k < nColors; k++){
    MyPalette[k] = FI+k;
  }
  gStyle->SetNumberContours(nColors);
  gStyle->SetPalette(nColors, MyPalette);
  boost::filesystem::path fitpath("fit");
  boost::filesystem::path yposfilename(m_outputname+".txt");
  boost::filesystem::path yposfile = outpath/fitpath/yposfilename;  
  boost::filesystem::path lengthfilename(m_outputname+"_length.txt");
  boost::filesystem::path lengthfile = outpath/fitpath/lengthfilename;

  Parser* m_Parser = new Parser();
  Parser::Param_Result_Map* YPos   = m_Parser->CreateParamResultMapFromFitFile( yposfile.c_str() );
  Parser::Param_Result_Map* Length = m_Parser->CreateParamResultMapFromFitFile( lengthfile.c_str() );
  //Parser::XYZ_Pos_Map*      XYZPos = m_Parser->CreateXYZPosMapFromFile( m_dbfilename );

  ST2DPlot* h_YPos         = new ST2DPlot("IT", "h_YPos",        Titlename_YPos);
  ST2DPlot* h_Length       = new ST2DPlot("IT", "h_Length",      "Measured - Expected Active Length");
  ST2DPlot* h_Length_Long  = new ST2DPlot("IT", "h_Length_Long", Titlename_Length);
  ST2DPlot* h_Length_Short = new ST2DPlot("IT", "h_Length_Short",Titlename_Length);
  gStyle->SetOptStat(1110);
  gStyle->SetStatY(0.87);
  gStyle->SetStatX(0.87);
  gStyle->SetStatH(0.30);
  gStyle->SetStatW(0.25);
  TH1D* h_YPos_             = new TH1D("h_YPos_",';'+Titlename_YPos+" [mm];",100,-Scale,Scale);
  TH1D* h_YPos_Box_         = new TH1D("h_YPos_Box_",';'+Titlename_YPos+" [mm];",100,-Scale,Scale);
  TH1D* h_Length_Long_      = new TH1D("h_Length_Long_","IT Long Sectors;"+Titlename_YPos+" [mm];",100,Length_Long-Scale,Length_Long+Scale);
  TH1D* h_Length_Short_     = new TH1D("h_Length_Short_","IT Short Sectors;"+Titlename_YPos+" [mm];",100,Length_Short-Scale,Length_Short+Scale);
  TH1D* h_Length_Long_Box_  = new TH1D("h_Length_Long_Box_","IT Long Sectors;"+Titlename_YPos+" [mm];",100,Length_Long-Scale,Length_Long+Scale);
  TH1D* h_Length_Short_Box_ = new TH1D("h_Length_Short_Box_","IT Short Sectors;"+Titlename_YPos+" [mm];",100,Length_Short-Scale,Length_Short+Scale);
  
  
  RooRealVar y("y","Local Y",-1000.,1000.,"mm");
  RooRealVar mean("mean","Mean",0.,-1000.,1000.,"mm");
  RooRealVar sigma("sigma","#sigma",0.5,0.,10.,"mm");
  RooGaussian* gauss = new RooGaussian("gauss","gauss",y,mean,sigma);
  
  std::map< std::string, TH1D* > hmap_YPos_Global;
  std::map< std::string, TH1D* > hmap_YPos_Box;
  std::map< std::string, TH1D* > hmap_YPos_BiLayer;
  std::map< std::string, TH1D* > hmap_Length_Box;

  std::map< std::string, Gauss_Fit_Result* > gfrmap_YPos_Global;
  std::map< std::string, Gauss_Fit_Result* > gfrmap_YPos_Box;
  std::map< std::string, Gauss_Fit_Result* > gfrmap_YPos_BiLayer;
  
  STNames::Vector* BoxNames;
  BoxNames = m_Names->GetBoxes();
  STNames::Vector* BiLayerNames;
  BiLayerNames = m_Names->GetBiLayers();
  STNames::SectNames* SectNames;
  
  string tmp;
  TString Ttmp;
  
  tmp = "h_YPos_";
  tmp += m_Names->GetGlobalName();
  hmap_YPos_Global[m_Names->GetGlobalName()] = new TH1D(tmp.c_str(),"",100,-Scale,Scale);
  //new hmap_Length_Box for each sector  
  for(STNames::Vector::iterator It2 = BoxNames->begin(); It2 != BoxNames->end(); It2++){
    tmp = "h_YPos_";
    tmp += (*It2);
    hmap_YPos_Box[*It2] = new TH1D(tmp.c_str(),"",100,-Scale,Scale);
    tmp = "h_Length_";
    tmp += (*It2);
    Ttmp = (*It2);
    if(Ttmp.Contains("Top") || Ttmp.Contains("Bottom"))
      hmap_Length_Box[*It2] = new TH1D(tmp.c_str(),"",100,Length_Short-Scale,Length_Short+Scale);
    else
      hmap_Length_Box[*It2] = new TH1D(tmp.c_str(),"",100,Length_Long-Scale,Length_Long+Scale);
  }
  //new hmap_YPos_BiLayer for each sector 
  for(STNames::Vector::iterator It2 = BiLayerNames->begin(); It2 != BiLayerNames->end(); It2++)
  {
    tmp = "h_YPos_";
    tmp += (*It2);
    hmap_YPos_BiLayer[*It2] = new TH1D(tmp.c_str(),"",100,-Scale,Scale);
  }
 
  //add corrections to the values for U and V layers
  for(Parser::Param_Result_Map::const_iterator It = YPos->begin(); It != YPos->end(); It++)
  {
    SectNames = m_Names->GetSectNames(It->first);
    if(!SectNames) continue;
    
    if(SectNames->Layer == 'U' || SectNames->Layer == 'V')
      It->second->Value = It->second->Value * TMath::Cos(5.*TMath::Pi()/180.);
  }
  
  for(Parser::Param_Result_Map::const_iterator It = YPos->begin(); It != YPos->end(); It++)
  {
    SectNames = m_Names->GetSectNames(It->first);
    if(!SectNames) continue;

    //exclude Sector3 & 5 for Top & Bottom boxes
    //std::cout << "sector name: " << It->first << std::endl;
    //TString SectorName = It->first;  
    //if( ( (SectorName.Contains("Top")&&(SectorName.Contains("Sector3")||SectorName.Contains("Sector5"))) || (SectorName.Contains("Bottom")&&(SectorName.Contains("Sector3")||SectorName.Contains("Sector5")))) ) continue;
    //std::cout << "-----> okay" << std::endl;
    
    h_YPos->Fill(It->first,It->second->Value);
    h_YPos_->Fill(It->second->Value);
    hmap_YPos_BiLayer[m_Names->GetBiLayerName(It->first)]->Fill(It->second->Value);
    hmap_YPos_Box[m_Names->GetBoxName(It->first)]->Fill(It->second->Value);
    hmap_YPos_Global[m_Names->GetGlobalName()]->Fill(It->second->Value);
  }

  //TGraphErrors* g_Y_Z = new TGraphErrors();
  //Int_t g_Y_Z_i(0.);
  for(Parser::Param_Result_Map::const_iterator It = Length->begin(); It != Length->end(); It++)
  {
    SectNames = m_Names->GetSectNames(It->first);
    if(!SectNames) continue;

    /*  
    //if(SectNames->Box != "Top" && SectNames->Box != "Bottom"){
    if(SectNames->Box != "ASide" && SectNames->Box != "CSide"){
      g_Y_Z->SetPoint(g_Y_Z_i,(*XYZPos)[It->first]->Z,It->second->Value);
      g_Y_Z->SetPointError(g_Y_Z_i,0.,It->second->Error);
      g_Y_Z_i++;
    }*/
    
    if(SectNames->Nickname.Contains("Top") || SectNames->Nickname.Contains("Bottom")){
      h_Length->Fill(It->first,It->second->Value-Length_Short);
      h_Length_Short->Fill(It->first,It->second->Value);
      h_Length_Short_->Fill(It->second->Value);
      h_Length_Long->Fill(It->first,0.0001);
    }else{
      h_Length->Fill(It->first,It->second->Value-Length_Long);
      h_Length_Long->Fill(It->first,It->second->Value);
      h_Length_Long_->Fill(It->second->Value);      
      h_Length_Short->Fill(It->first,0.0001);
    }
    hmap_Length_Box[m_Names->GetBoxName(It->first)]->Fill(It->second->Value);
  }

  for(STNames::Vector::iterator It2 = BoxNames->begin(); It2 != BoxNames->end(); It2++)
  {
    h_YPos_Box_->Fill(hmap_YPos_Box[*It2]->GetMean());

    if(hmap_Length_Box[*It2]->GetMean()<180.)
      h_Length_Short_Box_->Fill(hmap_Length_Box[*It2]->GetMean());
    else
      h_Length_Long_Box_->Fill(hmap_Length_Box[*It2]->GetMean());
  }
  
  cout << "######## Y Misalignment ########" << endl;
  
  if(hmap_YPos_Global[m_Names->GetGlobalName()]->GetEntries() > 0){
    gfrmap_YPos_Global[m_Names->GetGlobalName()] = fit_Gauss(hmap_YPos_Global[m_Names->GetGlobalName()],
                                                           (RooGaussian*)gauss->Clone("_gauss"),
                                                           m_Names->GetGlobalName().c_str(),0.,Scale);
    cout << *gfrmap_YPos_Global[m_Names->GetGlobalName()] << endl;
  }
  cout << endl;
  for(STNames::Vector::iterator It2 = BoxNames->begin(); It2 != BoxNames->end(); It2++)
  {
    if(hmap_YPos_Box[*It2]->GetEntries() > 0){
      gfrmap_YPos_Box[*It2] = fit_Gauss(hmap_YPos_Box[*It2],
                                        (RooGaussian*)gauss->Clone("_gauss"),
                                        (*It2).c_str(),
                                        0.,Scale);
      cout << *gfrmap_YPos_Box[*It2] << endl;
    }
  }
  cout << endl;
  for(STNames::Vector::iterator It2 = BiLayerNames->begin(); It2 != BiLayerNames->end(); It2++)
  {
    if(hmap_YPos_BiLayer[*It2]->GetEntries() > 0){
      gfrmap_YPos_BiLayer[*It2] = fit_Gauss(hmap_YPos_BiLayer[*It2],
                                            (RooGaussian*)gauss->Clone("_gauss"),
                                            (*It2).c_str(),
                                            0.,Scale);
      cout << *gfrmap_YPos_BiLayer[*It2] << endl;
    }
  }
  cout << endl;
  
  cout << "######## Active Length ########" << endl;
  
  for(STNames::Vector::iterator It2 = BoxNames->begin(); It2 != BoxNames->end(); It2++)
  {
    if(hmap_Length_Box[*It2]->GetEntries() > 0){
      Ttmp = (*It2);
      if(Ttmp.Contains("Top") || Ttmp.Contains("Bottom"))
        cout << *fit_Gauss(hmap_Length_Box[*It2],(RooGaussian*)gauss->Clone("_gauss"),(*It2).c_str(),Length_Short,Scale) << endl;
      else
        cout << *fit_Gauss(hmap_Length_Box[*It2],(RooGaussian*)gauss->Clone("_gauss"),(*It2).c_str(),Length_Long,Scale) << endl;
    }
  }
  
  cout << endl;

  TPaveText *lhcb7TeVPrelimR = new TPaveText(0.17, 0.75, 0.44, 0.88, "BRNDC");
  lhcb7TeVPrelimR->SetFillColor(0);
  lhcb7TeVPrelimR->SetTextAlign(12);
  lhcb7TeVPrelimR->SetBorderSize(0);
  lhcb7TeVPrelimR->AddText("LHCb Preliminary 2012");
  
  TCanvas* c_YPos = new TCanvas("c_YPos","c_YPos",1200,1000);
  
  h_YPos_->SetTitle(Titlename_YPos);
  h_YPos_->SetFillStyle(3004);
  h_YPos_->SetFillColor(2);
  h_YPos_->SetLineColor(2);
  h_YPos_->Draw();
  //lhcb7TeVPrelimR->Draw();
  boost::filesystem::path plotname(m_outputname+".eps");
  c_YPos->SaveAs( (outpath/plotpath/plotname).c_str() );

  TCanvas* c_YPos_Box = new TCanvas("c_YPos_Box","c_YPos_Box",1200,1000);
  
  h_YPos_Box_->SetTitle(Titlename_YPos);
  h_YPos_Box_->SetFillStyle(3004);
  h_YPos_Box_->SetFillColor(2);
  h_YPos_Box_->SetLineColor(2);
  h_YPos_Box_->Draw();
  //lhcb7TeVPrelimR->Draw();
  
  //c_YPos_Box->SaveAs("plots/"+outputname+"_Box.eps");
  plotname = boost::filesystem::path(m_outputname+"_Box.eps");
  c_YPos_Box->SaveAs( (outpath/plotpath/plotname).c_str() );

  TCanvas* c_YPos_2D = new TCanvas("c_YPos_2D","c_YPos_2D",1200,600);
  
  h_YPos->Histogram()->SetMinimum(-Scale);
  h_YPos->Histogram()->SetMaximum(Scale);
  h_YPos->Draw("COLZ");
  h_YPos->Draw("TEXT,same");
    
  //c_YPos_2D->SaveAs("plots/"+outputname+"_2D.eps");
  plotname = boost::filesystem::path(m_outputname+"_2D.eps");
  c_YPos_2D->SaveAs( (outpath/plotpath/plotname).c_str() );
  
  TCanvas* c_Length = new TCanvas("c_Length","c_Length",1200,600);
  
  TPad* p_Length = static_cast<TPad*>(gPad);
  p_Length->Divide(2,1);
  p_Length->cd(1);
  h_Length_Short_->Draw();
  h_Length_Short_->SetFillStyle(3004);
  h_Length_Short_->SetFillColor(2);
  h_Length_Short_->SetLineColor(2);
  TLine* line_Length_Short = new TLine(Length_Short,0,Length_Short,h_Length_Short_->GetMaximum()*0.95);
  line_Length_Short->SetLineColor(kBlue);
  line_Length_Short->SetLineWidth(3.);
  line_Length_Short->Draw();
  //lhcb7TeVPrelimR->Draw();
  p_Length->cd(2);
  h_Length_Long_->Draw();
  TLine* line_Length_Long = new TLine(Length_Long,0,Length_Long,h_Length_Long_->GetMaximum()*0.95);
  line_Length_Long->SetLineColor(kBlue);
  line_Length_Long->SetLineWidth(3.);
  line_Length_Long->Draw();
  //lhcb7TeVPrelimR->Draw();
  h_Length_Long_->SetFillStyle(3004);
  h_Length_Long_->SetFillColor(2);
  h_Length_Long_->SetLineColor(2);

  //c_Length->SaveAs("plots/"+outputname+"_length.eps");
  plotname = boost::filesystem::path(m_outputname+"_length.eps");
  c_Length->SaveAs( (outpath/plotpath/plotname).c_str() );
  
  TCanvas* c_Length_Box = new TCanvas("c_Length_Box","c_Length_Box",1200,600);
  
  TPad* p_Length_Box = static_cast<TPad*>(gPad);
  p_Length_Box->Divide(2,1);
  p_Length_Box->cd(1);
  h_Length_Short_Box_->Draw();
  h_Length_Short_Box_->SetFillStyle(3004);
  h_Length_Short_Box_->SetFillColor(2);
  h_Length_Short_Box_->SetLineColor(2);
  TLine* line_Length_Short_Box = new TLine(Length_Short,0,Length_Short,h_Length_Short_Box_->GetMaximum()*0.95);
  line_Length_Short_Box->SetLineColor(kBlue);
  line_Length_Short_Box->SetLineWidth(3.);
  line_Length_Short_Box->Draw();
  //lhcb7TeVPrelimR->Draw();
  p_Length_Box->cd(2);
  h_Length_Long_Box_->Draw();
  TLine* line_Length_Long_Box = new TLine(Length_Long,0,Length_Long,h_Length_Long_Box_->GetMaximum()*0.95);
  line_Length_Long_Box->SetLineColor(kBlue);
  line_Length_Long_Box->SetLineWidth(3.);
  line_Length_Long_Box->Draw();
  //lhcb7TeVPrelimR->Draw();
  h_Length_Long_Box_->SetFillStyle(3004);
  h_Length_Long_Box_->SetFillColor(2);
  h_Length_Long_Box_->SetLineColor(2);
  
  //c_Length_Box->SaveAs("plots/"+outputname+"_length_Box.eps");
  plotname = boost::filesystem::path(m_outputname+"_length_Box.eps");
  c_Length_Box->SaveAs( (outpath/plotpath/plotname).c_str() );
  
  TCanvas* c_Length_2D = new TCanvas("c_Length_2D","c_Length_2D",1200,600);

  h_Length->Histogram()->SetMinimum(-Scale);
  h_Length->Histogram()->SetMaximum(+Scale);
  h_Length->Draw("COLZ");
  h_Length->Draw("TEXT,same");

  //c_Length_2D->SaveAs("plots/"+outputname+"_length_2D.eps");
  plotname = boost::filesystem::path(m_outputname+"_length_2D.eps");
  c_Length_2D->SaveAs( (outpath/plotpath/plotname).c_str() );

}
