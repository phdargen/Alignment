#include "TVerticalAlignment/OTYAlignMagOff.h"
#include "boost/assign.hpp"
#include <boost/filesystem.hpp>

using namespace RooFit;
using namespace ROOT;
using namespace Alignment::TVerticalAlignment;
class OTNames;

OTYAlignMagOff::OTYAlignMagOff(TString filename,TString dbfilename, TString outputdir, bool constraint, bool saveplots) : 
    m_filename(filename), m_dbfilename(dbfilename), m_outDirectory(outputdir), m_constraint(constraint), m_saveplots(saveplots)
{

  std::cout << " init the TVerticalAlignment " << std::endl;

  m_va = new TVerticalAlignment();
  m_va->initParameter(std::string("OT"));
  m_param = m_va->getParameter(std::string("OT"));
  m_Names = new OTNames();

  std::cout << " read the coordinate files " << std::endl;

  m_GlobalXCoord = CreateXMapFromFile();
  m_GlobalYCoord = CreateYMapFromFile();

  std::cout << " OTYAlignMagOff class constructor done" << std::endl;

  if(!boost::filesystem::exists(m_outDirectory.Data())) boost::filesystem::create_directories(m_outDirectory.Data());

  boost::filesystem::path s(m_filename.Data());
  m_outputname = "OTYAlign_"+s.filename().string()+"_eff";
  m_outputname.ReplaceAll(".root","");
}

void OTYAlignMagOff::glimpse_data(){m_glimpse = true;}

RooDataSet* OTYAlignMagOff::GetRooDataSetFromTH1(TH1F* histoA1, TH1F* histoA1_exp, TH1F* histoA2, TH1F* histoA2_exp, 
                                 TH1F* histoB1, TH1F* histoB1_exp, TH1F* histoB2, TH1F* histoB2_exp, 
                                 RooRealVar y, RooRealVar weight,
                                 RooCategory cat_lr,  RooCategory cat_AB, RooCategory cat_eff,
                                 double Distance, double Length, double EdgeScale
                                 )
{

  double center;
  RooDataSet* data = new RooDataSet("data","data",RooArgSet(y,weight,cat_lr,cat_AB,cat_eff),WeightVar(weight));
  for(int i(1); i <= histoA1->GetNbinsX(); i++){
    center = histoA1->GetBinCenter(i);
    y.setVal(center);
    if(center < Distance/2.-Length/2.-EdgeScale*1.1 || center > Distance/2.-Length/2.+EdgeScale*1.1) continue;
    cat_lr.setLabel("left");
    cat_AB.setLabel("A");
    cat_eff.setLabel("found");
    data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoA1->GetBinContent(i));
  }
  
  for(int i(1); i <= histoA1_exp->GetNbinsX(); i++){
    center = histoA1_exp->GetBinCenter(i);
    y.setVal(center);
    if(center < Distance/2.-Length/2.-EdgeScale*1.1 || center > Distance/2.-Length/2.+EdgeScale*1.1) continue;
    cat_lr.setLabel("left");
    cat_AB.setLabel("A");
    cat_eff.setLabel("not found");
    
    for(int j(1); j <= histoA1->GetNbinsX(); j++){
      if(TMath::Abs(histoA1_exp->GetBinCenter(i)-histoA1->GetBinCenter(j)) > histoA1->GetBinWidth(1)/2.)
        continue;
      if(histoA1_exp->GetBinContent(i)-histoA1->GetBinContent(j) > -1)
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoA1_exp->GetBinContent(i)-histoA1->GetBinContent(j));
      else
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),0);
      break;
    }
  }
  
  for(int i(1); i <= histoA2->GetNbinsX(); i++){
    center = histoA2->GetBinCenter(i);
    y.setVal(center);
    
    if(center < Distance/2.+Length/2.-EdgeScale*1.1 || center > Distance/2.+Length/2.+EdgeScale*1.1)
      continue;
    cat_lr.setLabel("right");
    cat_AB.setLabel("A");
    cat_eff.setLabel("found");
    
    data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoA2->GetBinContent(i));
  }
  
  for(int i(1); i <= histoA2_exp->GetNbinsX(); i++){
    center = histoA2_exp->GetBinCenter(i);
    y.setVal(center);
    if(center < Distance/2.+Length/2.-EdgeScale*1.1 || center > Distance/2.+Length/2.+EdgeScale*1.1) continue;
    cat_lr.setLabel("right");
    cat_AB.setLabel("A");
    cat_eff.setLabel("not found");
    
    for(int j(1); j <= histoA2->GetNbinsX(); j++){
      if(TMath::Abs(histoA2_exp->GetBinCenter(i)-histoA2->GetBinCenter(j)) > histoA2->GetBinWidth(1)/2.)
        continue;
      if(histoA2_exp->GetBinContent(i)-histoA2->GetBinContent(j) > -1)
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoA2_exp->GetBinContent(i)-histoA2->GetBinContent(j));
      else
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),0);
      break;
    }
  }

  for(int i(1); i <= histoB1->GetNbinsX(); i++){
    center = histoB1->GetBinCenter(i);
    y.setVal(center);
    if(center < -Distance/2.-Length/2.-EdgeScale*1.1 || center > -Distance/2.-Length/2.+EdgeScale*1.1) continue;
    cat_lr.setLabel("left");
    cat_AB.setLabel("B");
    cat_eff.setLabel("found");
    data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoB1->GetBinContent(i));
  }
  
  for(int i(1); i <= histoB1_exp->GetNbinsX(); i++){
    center = histoB1_exp->GetBinCenter(i);
    y.setVal(center);
    if(center < -Distance/2.-Length/2.-EdgeScale*1.1 || center > -Distance/2.-Length/2.+EdgeScale*1.1) continue;
    cat_lr.setLabel("left");
    cat_AB.setLabel("B");
    cat_eff.setLabel("not found");
    
    for(int j(1); j <= histoB1->GetNbinsX(); j++){
      if(TMath::Abs(histoB1_exp->GetBinCenter(i)-histoB1->GetBinCenter(j)) > histoB1->GetBinWidth(1)/2.) continue;
      if(histoB1_exp->GetBinContent(i)-histoB1->GetBinContent(j) > -1)
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoB1_exp->GetBinContent(i)-histoB1->GetBinContent(j));
      else
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),0);
      break;
    }
  }
  
  for(int i(1); i <= histoB2->GetNbinsX(); i++){
    center = histoB2->GetBinCenter(i);
    y.setVal(center);
    if(center < -Distance/2.+Length/2.-EdgeScale*1.1 || center > -Distance/2.+Length/2.+EdgeScale*1.1) continue;
    cat_lr.setLabel("right");
    cat_AB.setLabel("B");
    cat_eff.setLabel("found");
    data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoB2->GetBinContent(i));
  }
  
  for(int i(1); i <= histoB2_exp->GetNbinsX(); i++){
    center = histoB2_exp->GetBinCenter(i);
    y.setVal(center);
    if(center < -Distance/2.+Length/2.-EdgeScale*1.1 || center > -Distance/2.+Length/2.+EdgeScale*1.1) continue;
    cat_lr.setLabel("right");
    cat_AB.setLabel("B");
    cat_eff.setLabel("not found");
    
    for(int j(1); j <= histoB2->GetNbinsX(); j++){
      if(TMath::Abs(histoB2_exp->GetBinCenter(i)-histoB2->GetBinCenter(j)) > histoB2->GetBinWidth(1)/2.) continue;
      if(histoB2_exp->GetBinContent(i)-histoB2->GetBinContent(j) > -1)
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),histoB2_exp->GetBinContent(i)-histoB2->GetBinContent(j));
      else
        data->add(RooArgSet(y,cat_lr,cat_AB,cat_eff),0);
      break;
    }
  }
  
  return data;
}

double OTYAlignMagOff::GetShift(TString ModuleName,TString ModuleName2)
{
  double _Length;
  double _Angle;
  
  if(ModuleName.Contains("X"))
    _Angle = 0.;
  else if(ModuleName.Contains("U"))
    _Angle = 5.;
  else
    _Angle = -5.;


  cout << ModuleName << " " << ModuleName2;
  cout << "deltaX = " << m_GlobalXCoord[ModuleName2.Data()]-m_GlobalXCoord[ModuleName.Data()] << ", deltaY = "<<m_GlobalYCoord[ModuleName2.Data()]-m_GlobalYCoord[ModuleName.Data()] << endl; 
  _Length = (m_GlobalXCoord[ModuleName2.Data()]-m_GlobalXCoord[ModuleName.Data()])*TMath::Sin(_Angle*TMath::Pi()/180.);
  cout  << " " << _Length;
  _Length += (m_GlobalYCoord[ModuleName2.Data()]-m_GlobalYCoord[ModuleName.Data()])*TMath::Cos(_Angle*TMath::Pi()/180.);
  cout  << " " << _Length << endl;
  
  return _Length/2.;
}

std::map<std::string, double> OTYAlignMagOff::CreateXMapFromFile()
{
  ifstream NullCoord(m_dbfilename);
  string Str("");
  TString Str_Sector("");
  TString TStr_X("");
  std::map<std::string, double> GlobalXCoord;
      
  while(!NullCoord.eof()){
    Str_Sector = "";
    TStr_X = "";
    getline(NullCoord,Str);
    
    unsigned int i(0);
    for(; i < Str.size() ; ++i){
      if(Str[i] != ' ')
        Str_Sector += Str[i];
      else
        break;
    }
    i++;
    i++;
    for(; i < Str.size() ; ++i){
      if(Str[i] != ',')
        TStr_X += Str[i];
      else
        break;
    }
    Str_Sector.ReplaceAll("/","");
    
    GlobalXCoord[Str_Sector.Data()] = TStr_X.Atof();
  }
  return GlobalXCoord;
}

std::map<std::string, double> OTYAlignMagOff::CreateYMapFromFile()
{
  ifstream NullCoord(m_dbfilename);
  string Str("");
  TString Str_Sector("");
  TString TStr_Y("");
  std::map<std::string, double> GlobalYCoord;
      
  while(!NullCoord.eof()){
    Str_Sector = "";
    TStr_Y = "";
    getline(NullCoord,Str);
    
    unsigned int i(0);
    for(; i < Str.size() ; ++i){
      if(Str[i] != ' ')
        Str_Sector += Str[i];
      else
        break;
    }
    i++;
    i++;
    for(;i<Str.size();++i){
    	if(Str[i] != ',')
        	;
    	else
        	break;
    }
    i++;
    for(; i < Str.size() ; ++i){
      if(Str[i] != ',')
        TStr_Y += Str[i];
      else
        break;
    }
    Str_Sector.ReplaceAll("/","");

    GlobalYCoord[Str_Sector.Data()] = TStr_Y.Atof();
  }
  return GlobalYCoord;
}

void OTYAlignMagOff::GetMeanFromHisto(TString ModuleName)
{
  std::cout << " call function GetMeanFromHisto() for sector " <<  ModuleName << std::endl;
  boost::filesystem::path outpath(m_outDirectory);
  boost::filesystem::path plotpath("plots/"+m_outputname);
  if(m_saveplots){
    if(!boost::filesystem::exists(outpath/plotpath)) boost::filesystem::create_directories(outpath/plotpath);
  }

  // debug zhirui
  //if( !( ModuleName.Contains("M8") || ModuleName.Contains("M9") ) ) return;

  TString _A("A");
  TString _B("B");
  TString _AEXP("");
  TString _BEXP("");

  TString ModuleName2( ModuleName );
  ModuleName2.ReplaceAll("Q0","Q2");
  ModuleName2.ReplaceAll("Q1","Q3");
  
  TString _ModuleName( ModuleName );
  TString _ModuleName2( ModuleName2 );
  
  _ModuleName.ReplaceAll("/","");
  _ModuleName2.ReplaceAll("/","");
  
  TString ModuleName_OTNames( 'O' );
  ModuleName_OTNames += _ModuleName;

  auto m_OT = boost::get<OTParam>(m_param);
  double ParTol   = m_OT.tolerance();
  double SigmaConstraint = m_OT.sigmaconstraint();
  double Distance, Length, EdgeScale;
  auto   itt_distance = m_OT.distance().find(std::string("F"));
  Distance = itt_distance->second;
  auto itt_length = m_OT.length().find(std::string("F"));
  Length = itt_length->second;
  auto itt_edge = m_OT.edge().find(std::string("F"));
  EdgeScale  = itt_edge->second;
  EdgeScale -= (m_Names->GetModuleNames(ModuleName_OTNames.Data())->Moduleno -5)*10.;
          
  if(ModuleName.Contains("M8")){
    auto itt_distance = m_OT.length().find(std::string("S1"));
    if ( m_OT.distance().end()!=itt_distance ) Distance  = itt_distance->second;
    auto itt_length = m_OT.length().find(std::string("S1"));
    if (m_OT.length().end()!=itt_length) Length = itt_length->second;
    auto itt_edge = m_OT.edge().find(std::string("S1"));
    if (m_OT.edge().end()!=itt_edge) EdgeScale = itt_edge->second;
  }

  if(ModuleName.Contains("M9")){

    auto itt_distance = m_OT.length().find(std::string("S23"));
    if ( m_OT.distance().end()!=itt_distance ) Distance  = itt_distance->second;
    auto itt_length = m_OT.length().find(std::string("S23"));
    if (m_OT.length().end()!=itt_length) Length = itt_length->second;
    auto itt_edge = m_OT.edge().find(std::string("S23"));
    if (m_OT.edge().end()!=itt_edge) EdgeScale = itt_edge->second;

    auto itt_step = m_OT.step().find(std::string("S23"));
    if( ModuleName.Contains("T1") ){
      if(m_OT.step().end() != itt_step) Length -= itt_step->second;
    }else if(ModuleName.Contains("T3") ){
      if(m_OT.step().end() != itt_step) Length += itt_step->second;
    }
  }
  
  if(ModuleName.Contains("X1") && Distance < 0.)
    Distance *= -1.;
  if(ModuleName.Contains("U") && Distance > 0.)
    Distance *= -1.;
  if(ModuleName.Contains("V") && Distance < 0.)
    Distance *= -1.;
  if(ModuleName.Contains("X2") && Distance > 0.)
    Distance *= -1.;

  if(ModuleName.Contains("T2/X2/Q0/M5") && !m_OT.mc() )
    Distance *= -1.;

  std::cout << " done for the constant parameters " << std::endl;
  //read the histograms
 
  TString directory(m_OT.directory().first);
  TString directory_exp(m_OT.directory().second);

  std::cout << " directories " << directory << " " << directory_exp << std::endl;
 
  TFile* datafile = new TFile(m_filename);
  std::cout << " read histogram " << (TString)(directory+ModuleName+_A) << " from file " << m_filename << std::endl;
  TH1F* _yeffA1     = (TH1F*)datafile->Get((TString)(directory+ModuleName+_A));
  std::cout << " read histogram " << (TString)(directory_exp+ModuleName+_AEXP) << " from file " << m_filename << std::endl;
  TH1F* _yeffA1_exp = (TH1F*)datafile->Get((TString)(directory_exp+ModuleName+_AEXP));
  _yeffA1_exp->SetDirectory(0);

  TH1F* _yeffA2     = (TH1F*)datafile->Get((TString)(directory+ModuleName2+_A));
  TH1F* _yeffA2_exp = (TH1F*)datafile->Get((TString)(directory_exp+ModuleName2+_AEXP));
  _yeffA2_exp->SetDirectory(0);

  TH1F* _yeffB1     = (TH1F*)datafile->Get((TString)(directory+ModuleName+_B));
  TH1F* _yeffB1_exp = (TH1F*)datafile->Get((TString)(directory_exp+ModuleName+_BEXP));
  _yeffB1_exp->SetDirectory(0);

  TH1F* _yeffB2     = (TH1F*)datafile->Get((TString)(directory+ModuleName2+_B));
  TH1F* _yeffB2_exp = (TH1F*)datafile->Get((TString)(directory_exp+ModuleName2+_BEXP));
  _yeffB2_exp->SetDirectory(0);

  std::cout << " Efficiency of  " << (TString)(directory+ModuleName+_A) << " over " << (TString)(directory_exp+ModuleName+_AEXP) << std::endl;
  std::cout << " Efficiency of  " << (TString)(directory+ModuleName2+_A) << " over " << (TString)(directory_exp+ModuleName2+_AEXP) << std::endl;
  std::cout << " Efficiency of  " << (TString)(directory+ModuleName+_B) << " over " << (TString)(directory_exp+ModuleName+_BEXP) << std::endl;
  std::cout << " Efficiency of  " << (TString)(directory+ModuleName2+_B) << " over " << (TString)(directory_exp+ModuleName2+_BEXP) << std::endl;

  if(!_yeffA1 || !_yeffA1_exp) return;
  if(!_yeffA2 || !_yeffA2_exp) return;
  if(!_yeffB1 || !_yeffB1_exp) return;
  if(!_yeffB2 || !_yeffB2_exp) return;

  double YShift;
  YShift = GetShift(_ModuleName.Data(),_ModuleName2.Data());
  cout << "YShift = " << YShift << " between " << _ModuleName.Data() << ", " <<_ModuleName2.Data() << endl;

  cout << "_yeffA1 (min,max) = " << _yeffA1->GetXaxis()->GetXmin() << ","<< _yeffA1->GetXaxis()->GetXmax() << endl; 
  cout << "_yeffA1 (min,max) = " << _yeffA1->GetXaxis()->GetXmin()-YShift << ","<< _yeffA1->GetXaxis()->GetXmax()-YShift << endl;
  _yeffA1->GetXaxis()->Set(_yeffA1->GetXaxis()->GetNbins(),
                           _yeffA1->GetXaxis()->GetXmin()-YShift,
                           _yeffA1->GetXaxis()->GetXmax()-YShift);
 
  cout << "_yeffA1_exp (min,max) = " << _yeffA1_exp->GetXaxis()->GetXmin() << ","<< _yeffA1_exp->GetXaxis()->GetXmax() << endl; 
  cout << "_yeffA1_exp (min,max) = " << _yeffA1_exp->GetXaxis()->GetXmin()-YShift << ","<< _yeffA1_exp->GetXaxis()->GetXmax()-YShift << endl; 
  _yeffA1_exp->GetXaxis()->Set(_yeffA1_exp->GetXaxis()->GetNbins(),
                               _yeffA1_exp->GetXaxis()->GetXmin()-YShift,
                               _yeffA1_exp->GetXaxis()->GetXmax()-YShift);
  
  cout << "_yeffA2 (min,max) = " << _yeffA2->GetXaxis()->GetXmin() << ","<< _yeffA2->GetXaxis()->GetXmax() << endl; 
  cout << "_yeffA2 (min,max) = " << _yeffA2->GetXaxis()->GetXmin()+YShift << ","<< _yeffA2->GetXaxis()->GetXmax()+YShift << endl; 
  _yeffA2->GetXaxis()->Set(_yeffA2->GetXaxis()->GetNbins(),
                           _yeffA2->GetXaxis()->GetXmin()+YShift,
                           _yeffA2->GetXaxis()->GetXmax()+YShift);
  
  cout << "_yeffA2_exp (min,max) = " << _yeffA2_exp->GetXaxis()->GetXmin() << ","<< _yeffA2_exp->GetXaxis()->GetXmax() << endl; 
  cout << "_yeffA2_exp (min,max) = " << _yeffA2_exp->GetXaxis()->GetXmin()+YShift << ","<< _yeffA2_exp->GetXaxis()->GetXmax()+YShift << endl; 
  _yeffA2_exp->GetXaxis()->Set(_yeffA2_exp->GetXaxis()->GetNbins(),
                               _yeffA2_exp->GetXaxis()->GetXmin()+YShift,
                               _yeffA2_exp->GetXaxis()->GetXmax()+YShift);
  
  cout << "_yeffB1 (min,max) = " << _yeffB1->GetXaxis()->GetXmin() << ","<< _yeffB1->GetXaxis()->GetXmax() << endl; 
  cout << "_yeffB1 (min,max) = " << _yeffB1->GetXaxis()->GetXmin()-YShift << ","<< _yeffB1->GetXaxis()->GetXmax()-YShift << endl; 
  _yeffB1->GetXaxis()->Set(_yeffB1->GetXaxis()->GetNbins(),
                           _yeffB1->GetXaxis()->GetXmin()-YShift,
                           _yeffB1->GetXaxis()->GetXmax()-YShift);
  
  cout << "_yeffB1_exp (min,max) = " << _yeffB1_exp->GetXaxis()->GetXmin() << ","<< _yeffB1_exp->GetXaxis()->GetXmax() << "," << _yeffB1_exp->GetXaxis()->GetNbins() << endl; 
  cout << "_yeffB1_exp (min,max) = " << _yeffB1_exp->GetXaxis()->GetXmin()-YShift << ","<< _yeffB1_exp->GetXaxis()->GetXmax()-YShift << endl; 
  _yeffB1_exp->GetXaxis()->Set(_yeffB1_exp->GetXaxis()->GetNbins(),
                           _yeffB1_exp->GetXaxis()->GetXmin()-YShift,
                           _yeffB1_exp->GetXaxis()->GetXmax()-YShift);
  
  cout << "_yeffB2 (min,max) = " << _yeffB2->GetXaxis()->GetXmin() << ","<< _yeffB2->GetXaxis()->GetXmax() << endl; 
  cout << "_yeffB2 (min,max) = " << _yeffB2->GetXaxis()->GetXmin()+YShift << ","<< _yeffB2->GetXaxis()->GetXmax()+YShift << endl;
  _yeffB2->GetXaxis()->Set(_yeffB2->GetXaxis()->GetNbins(),
                           _yeffB2->GetXaxis()->GetXmin()+YShift,
                           _yeffB2->GetXaxis()->GetXmax()+YShift);
  
  cout << "_yeffB2_exp (min,max) = " << _yeffB2_exp->GetXaxis()->GetXmin() << ","<< _yeffB2_exp->GetXaxis()->GetXmax() << "," << _yeffB2_exp->GetXaxis()->GetNbins() << endl; 
  cout << "_yeffB2_exp (min,max) = " << _yeffB2_exp->GetXaxis()->GetXmin()+YShift << ","<< _yeffB2_exp->GetXaxis()->GetXmax()+YShift << endl;
  _yeffB2_exp->GetXaxis()->Set(_yeffB2_exp->GetXaxis()->GetNbins(),
                           _yeffB2_exp->GetXaxis()->GetXmin()+YShift,
                           _yeffB2_exp->GetXaxis()->GetXmax()+YShift);
  
  if(_yeffA1 !=0 || _yeffA2 !=0 || _yeffB1 !=0 || _yeffB2 !=0){
    
    RooRealVar y("y","y",-500.,500.);
    y.setBins(100);
   
    cout << " module " << ModuleName << " : Distance(" << Distance << "), EdgeScale(" << EdgeScale << "), Length(" << Length<< ")" << endl;

    y.setRange("leftA",  Distance/2.-Length/2.-EdgeScale, Distance/2.-Length/2.+EdgeScale);
    y.setRange("rightA", Distance/2.+Length/2.-EdgeScale, Distance/2.+Length/2.+EdgeScale);
    y.setRange("leftB", -Distance/2.-Length/2.-EdgeScale,-Distance/2.-Length/2.+EdgeScale);
    y.setRange("rightB",-Distance/2.+Length/2.-EdgeScale,-Distance/2.+Length/2.+EdgeScale);

    if(ModuleName.Contains("M8") || ModuleName.Contains("M9") || ModuleName2.Contains("M8") || ModuleName2.Contains("M9")){
      y.setRange("leftA",   Distance/2.-Length/2.-EdgeScale-10, Distance/2.-Length/2.+EdgeScale-150);
      y.setRange("rightA",  Distance/2.+Length/2.-EdgeScale-10, Distance/2.+Length/2.+EdgeScale-150);
      y.setRange( "leftB", -Distance/2.-Length/2.-EdgeScale+150, -Distance/2.-Length/2.+EdgeScale+10);
      y.setRange("rightB", -Distance/2.+Length/2.-EdgeScale+150, -Distance/2.+Length/2.+EdgeScale+10);
    }

    cout << "leftA " <<  Distance/2.-Length/2.-EdgeScale <<", "<<  Distance/2.-Length/2.+EdgeScale << endl;
    cout << "rightA" <<  Distance/2.+Length/2.-EdgeScale <<", "<<  Distance/2.+Length/2.+EdgeScale << endl;
    cout << "leftB " << -Distance/2.-Length/2.-EdgeScale <<", "<< -Distance/2.-Length/2.+EdgeScale << endl;
    cout << "rightB" << -Distance/2.+Length/2.-EdgeScale <<", "<< -Distance/2.+Length/2.+EdgeScale << endl;
      
    RooCategory cat_lr("cat_lr","cat_lr");
    cat_lr.defineType("left",-1) ;
    cat_lr.defineType("right",1) ;
    RooCategory cat_AB("cat_AB","cat_AB");
    cat_AB.defineType("A",-1) ;
    cat_AB.defineType("B",1) ;
    RooCategory cat_eff("cat_eff","cat_eff");
    cat_eff.defineType("found",1) ;
    cat_eff.defineType("not found",0) ;
    RooRealVar weight("weight","weight",0,100000000000000);
    
    RooDataSet* yeff_data = 0;
    yeff_data = GetRooDataSetFromTH1(_yeffA1,_yeffA1_exp,_yeffA2,_yeffA2_exp,
                                     _yeffB1,_yeffB1_exp,_yeffB2,_yeffB2_exp,
                                     y,weight,cat_lr,cat_AB,cat_eff,
                                     Distance, Length, EdgeScale);

    RooDataSet* yeff_data_A = (RooDataSet*)yeff_data->reduce("cat_AB == cat_AB::A");
    RooDataSet* yeff_A_l = (RooDataSet*)yeff_data_A->reduce("cat_lr == cat_lr::left");
    RooDataSet* yeff_A_r = (RooDataSet*)yeff_data_A->reduce("cat_lr == cat_lr::right");
 
    RooDataSet* yeff_data_B = (RooDataSet*)yeff_data->reduce("cat_AB == cat_AB::B");
    RooDataSet* yeff_B_l = (RooDataSet*)yeff_data_B->reduce("cat_lr == cat_lr::left");
    RooDataSet* yeff_B_r = (RooDataSet*)yeff_data_B->reduce("cat_lr == cat_lr::right");

    //define models here

    RooRealVar center(    "center",  "center",0.,-5.,5.,"mm");
    RooRealVar distance("distance","Distance",100.,-500.,500.,"mm");
    RooRealVar length_A("length_A",  "Length",36.,10.,500.,"mm");
    RooRealVar length_B("length_B",  "Length",36.,10.,500.,"mm");

    center.setRange(0.-ParTol,0.+ParTol);
    center.setVal(0.);
    distance.setRange(Distance-ParTol,Distance+ParTol);
    distance.setVal(Distance);
    length_A.setRange(Length-ParTol,Length+ParTol);
    length_A.setVal(Length);
    length_B.setRange(Length-ParTol,Length+ParTol);
    length_B.setVal(Length);
  
    cout << "mu_A_l = " << Distance/2.-Length/2. << endl;

    /* Monolayer A Left  &  A Right*/ 
    RooRealVar N_A_l("N_A_l","N",0.05,0.99);
    RooFormulaVar mu_A_l("mu_A_l","center+distance/2.-length_A/2.",RooArgSet(center,length_A,distance));
    RooRealVar sigma_A_l("sigma_A_l","#sigma_{l}^{A}",1.,0.2,40.,"mm");
    RooFormulaVar y_A_l("y_A_l","(y-mu_A_l)/(TMath::Sqrt(2)*sigma_A_l)",RooArgSet(y,mu_A_l,sigma_A_l));
    RooRealVar sigslope_A_l("sigslope_A_l","Sig Slope",0.,-0.012,0.012,"");
    RooRealVar bkgslope_A_l("bkgslope_A_l","Bkg Slope",0.,-0.008,0.008,"");
    RooRealVar sigfrac_A_l("sigfrac_A_l","Sig Frac",0.5,0.01,0.9,"");
    N_A_l.setVal(0.5);
    sigma_A_l.setVal(1.);
    sigslope_A_l.setVal(0.);
    bkgslope_A_l.setVal(0.);
    sigfrac_A_l.setVal(0.6);
    sigslope_A_l.setConstant();
    bkgslope_A_l.setConstant();

    RooArgSet arg_commun_A(y,center,length_A,distance,cat_lr,cat_AB);
    RooArgSet arg_A_l(y_A_l,sigma_A_l,sigfrac_A_l,sigslope_A_l,bkgslope_A_l,N_A_l);
    arg_A_l.add(arg_commun_A);
    
    cout << "mu_A_r = " << Distance/2.+Length/2. << endl;

    RooRealVar N_A_r("N_A_r","N",0.05,0.99);
    RooFormulaVar mu_A_r("mu_A_r","center+distance/2.+length_A/2.",RooArgSet(center,length_A,distance));
    RooRealVar sigma_A_r("sigma_A_r","#sigma_{r}^{A}",1.,0.2,40.,"mm");
    RooFormulaVar y_A_r("y_A_r","(y-mu_A_r)/(TMath::Sqrt(2)*sigma_A_r)",RooArgSet(y,mu_A_r,sigma_A_r));
    RooRealVar sigslope_A_r("sigslope_A_r","Sig Slope",0.,-0.012,0.012,"");
    RooRealVar bkgslope_A_r("bkgslope_A_r","Bkg Slope",0.,-0.008,0.008,"");
    RooRealVar sigfrac_A_r("sigfrac_A_r","Sig Frac",0.5,0.01,0.9,"");
    N_A_r.setVal(0.5);
    sigma_A_r.setVal(10.);
    sigslope_A_r.setVal(0.);
    bkgslope_A_r.setVal(0.);
    sigfrac_A_r.setVal(0.4);
    sigslope_A_r.setConstant(); 
    bkgslope_A_r.setConstant(); 
    
    RooArgSet arg_A_r(y_A_r,sigma_A_r,sigfrac_A_r,sigslope_A_r,bkgslope_A_r,N_A_r);
    arg_A_r.add(arg_commun_A);
    
    cout << "mu_B_l = " << -Distance/2.-Length/2. << endl;
    /* Monolayer B Left  &  B Right*/ 
    RooRealVar N_B_l("N_B_l","N",0.05,0.99);
    RooFormulaVar mu_B_l("mu_B_l","center-distance/2.-length_B/2.",RooArgSet(center,length_B,distance));
    RooRealVar sigma_B_l("sigma_B_l","#sigma_{l}^{B}",1.,0.2,50.,"mm");
    RooFormulaVar y_B_l("y_B_l","(y-mu_B_l)/(TMath::Sqrt(2)*sigma_B_l)",RooArgSet(y,mu_B_l,sigma_B_l));
    RooRealVar sigslope_B_l("sigslope_B_l","Sig Slope",0.,-0.012,0.012,"");
    RooRealVar bkgslope_B_l("bkgslope_B_l","Bkg Slope",0.,-0.008,0.008,"");
    RooRealVar sigfrac_B_l("sigfrac_B_l","Sig Frac",0.5,0.01,0.9,"");
    N_B_l.setVal(0.5);
    sigma_B_l.setVal(1.);
    sigslope_B_l.setVal(0.);
    bkgslope_B_l.setVal(0.);
    sigfrac_B_l.setVal(0.6);
    sigslope_B_l.setConstant();
    bkgslope_B_l.setConstant();

    RooArgSet arg_commun_B(y,center,length_B,distance,cat_lr,cat_AB);    
    RooArgSet arg_B_l(y_B_l,sigma_B_l,sigfrac_B_l,sigslope_B_l,bkgslope_B_l,N_B_l);
    arg_B_l.add(arg_commun_B);

    cout << "mu_B_r = " << -Distance/2.+Length/2. << endl;

    RooRealVar N_B_r("N_B_r","N",0.05,0.99);
    RooFormulaVar mu_B_r("mu_B_r","center-distance/2.+length_B/2.",RooArgSet(center,length_B,distance));
    RooRealVar sigma_B_r("sigma_B_r","#sigma_{r}^{B}",1.,0.2,50.,"mm");
    RooFormulaVar y_B_r("y_B_r","(y-mu_B_r)/(TMath::Sqrt(2)*sigma_B_r)",RooArgSet(y,mu_B_r,sigma_B_r));
    RooRealVar sigslope_B_r("sigslope_B_r","Sig Slope",0.,-0.012,0.012,"");
    RooRealVar bkgslope_B_r("bkgslope_B_r","Bkg Slope",0.,-0.008,0.008,"");
    RooRealVar sigfrac_B_r("sigfrac_B_r","Sig Frac",0.5,0.01,0.9,"");
    N_B_r.setVal(0.5);
    sigma_B_r.setVal(10.);
    sigslope_B_r.setVal(0.);
    bkgslope_B_r.setVal(0.);
    sigfrac_B_r.setVal(0.4);
    sigslope_B_r.setConstant(); 
    bkgslope_B_r.setConstant();
 
    RooArgSet arg_B_r(y_B_r,sigma_B_r,sigfrac_B_r,sigslope_B_r,bkgslope_B_r,N_B_r);
    arg_B_r.add(arg_commun_B);
    // Formulas

    TString formula_A_l("N_A_l*(sigfrac_A_l*(1.+sigslope_A_l*(y-(center+distance/2.-length_A/2.)))*(0.5+0.5*TMath::Erf(-y_A_l))+(1.-sigfrac_A_l)*(1.+bkgslope_A_l*(y-(center+distance/2.-length_A/2.))))*(cat_lr == -1)*(cat_AB == -1)");
    TString formula_A_r("N_A_r*(sigfrac_A_r*(1.+sigslope_A_r*(y-(center+distance/2.+length_A/2.)))*(0.5+0.5*TMath::Erf(y_A_r))+(1.-sigfrac_A_r)*(1.+bkgslope_A_r*(y-(center+distance/2.+length_A/2.))))*(cat_lr == 1)*(cat_AB == -1)");
    
    TString formula_B_l("N_B_l*(sigfrac_B_l*(1.+sigslope_B_l*(y-(center-distance/2.-length_B/2.)))*(0.5+0.5*TMath::Erf(-y_B_l))+(1.-sigfrac_B_l)*(1.+bkgslope_B_l*(y-(center-distance/2.-length_B/2.))))*(cat_lr == -1)*(cat_AB == 1)");
    TString formula_B_r("N_B_r*(sigfrac_B_r*(1.+sigslope_B_r*(y-(center-distance/2.+length_B/2.)))*(0.5+0.5*TMath::Erf(y_B_r))+(1.-sigfrac_B_r)*(1.+bkgslope_B_r*(y-(center-distance/2.+length_B/2.))))*(cat_lr == 1)*(cat_AB == 1)");


    RooFormulaVar* _model_eff_A_l = new RooFormulaVar("_model_eff_A_l",formula_A_l,arg_A_l);
    RooEfficiency*  model_eff_A_l = new RooEfficiency( "model_eff_A_l","model_eff_A_l",*_model_eff_A_l,cat_eff,"found");
    RooFormulaVar* _model_eff_B_r = new RooFormulaVar("_model_eff_B_r",formula_B_r,arg_B_r);
    RooEfficiency*  model_eff_B_r = new RooEfficiency( "model_eff_B_r","model_eff_B_r",*_model_eff_B_r,cat_eff,"found");
    
    RooFormulaVar* _model_eff_B_l = new RooFormulaVar("_model_eff_B_l",formula_B_l,arg_B_l);
    RooEfficiency*  model_eff_B_l = new RooEfficiency( "model_eff_B_l","model_eff_B_l",*_model_eff_B_l,cat_eff,"found");
    RooFormulaVar* _model_eff_A_r = new RooFormulaVar("_model_eff_A_r",formula_A_r,arg_A_r);
    RooEfficiency*  model_eff_A_r = new RooEfficiency( "model_eff_A_r","model_eff_A_r",*_model_eff_A_r,cat_eff,"found");
    
    //Modules:
    // | AL(1)  | AR(1)  |
    // | BL(1)  | BR(2)  |
    // 2: Q2,3 (right)
    // 1: Q0,1 (left)
    // AL & BR are the longer monolayer
    // BL & AR are the shorter monolayer
    TString formula_A_lr(formula_A_l);
    formula_A_lr += "+";
    formula_A_lr += formula_A_r;
    TString formula_B_lr(formula_B_l);
    formula_B_lr += "+";
    formula_B_lr += formula_B_r;
    
    TString formula_lr(formula_A_lr);
    formula_lr += "+";
    formula_lr += formula_B_lr;    
    
    RooArgSet arg_A_lr(arg_A_l);
    arg_A_lr.add(arg_A_r);
    RooArgSet arg_B_lr(arg_B_l);
    arg_B_lr.add(arg_B_r);
    RooArgSet arg_lr(arg_A_lr);
    arg_lr.add(arg_B_lr);
    
    RooFormulaVar* _model_eff_A = new RooFormulaVar("_model_eff_A",formula_A_lr,arg_A_lr);
    RooEfficiency*  model_eff_A = new RooEfficiency("model_eff_A","model_eff",*_model_eff_A,cat_eff,"found");
    
    RooFormulaVar* _model_eff_B = new RooFormulaVar("_model_eff_B",formula_B_lr,arg_B_lr);
    RooEfficiency*  model_eff_B = new RooEfficiency("model_eff_B","model_eff",*_model_eff_B,cat_eff,"found");

    RooFormulaVar* _model_eff = new RooFormulaVar("_model_eff",formula_lr,arg_lr);
    RooEfficiency*  model_eff = new RooEfficiency("model_eff","model_eff",*_model_eff,cat_eff,"found");

    ModuleName.ReplaceAll("/","");
    ModuleName2.ReplaceAll("/","");
    if(!m_glimpse){
  
     ////////////////////////
     //Start to do the fit///
     ///////////////////////
      
      center.setConstant();
      
      //Step I
      
      length_A.setConstant();
      distance.setConstant(false);
      model_eff_A_l->fitTo(*yeff_A_l, Minos(true), Range("leftA"), PrintLevel(-1), 
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4));
      length_A.setConstant(false);
      distance.setConstant();
      model_eff_A_r->fitTo(*yeff_A_r, Minos(true), Range("rightA"), PrintLevel(-1), 
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4));
      length_A.setConstant(false);
      distance.setConstant(false);
    
      //Step II 
      length_B.setConstant();
      distance.setConstant(false);
      model_eff_B_l->fitTo(*yeff_B_l, Minos(true), Range("leftB"), PrintLevel(-1), 
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4));
      length_B.setConstant(false);
      distance.setConstant();
      model_eff_B_r->fitTo(*yeff_B_r, Minos(true), Range("rightB"), PrintLevel(-1), 
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4));
      length_B.setConstant(false);
      distance.setConstant(false);
      
      //Step III
      if(m_constraint){
        RooGaussian* fconstext_length_A = new RooGaussian("fconstext_length_A","fconstext_length_A",length_A,RooConst(Length),RooConst(SigmaConstraint)) ;
        model_eff_A->fitTo(*yeff_data_A, Minos(true), Range("leftA,rightA"), PrintLevel(-1), 
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4),
                           ExternalConstraints(*fconstext_length_A));
      }else{
        model_eff_A->fitTo(*yeff_data_A, Minos(true), Range("leftA,rightA"), PrintLevel(-1),
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4));
      }
          
      if(m_constraint){
        RooGaussian* fconstext_length_B = new RooGaussian("fconstext_length_B","fconstext_length_B",length_B,RooConst(Length),RooConst(SigmaConstraint)) ;
        model_eff_B->fitTo(*yeff_data_B, Minos(true), Range("leftB,rightB"), PrintLevel(-1), 
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4),
                           ExternalConstraints(*fconstext_length_B));
      }else{
        model_eff_B->fitTo(*yeff_data_B, Minos(true), Range("leftB,rightB"), PrintLevel(-1),
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4));
      }
  
          
      center.setConstant(false);
      distance.setConstant(false);
  
      length_A.setConstant();
      length_B.setConstant();
  
      N_A_l.setConstant();
      N_A_r.setConstant();
      N_B_l.setConstant();
      N_B_r.setConstant();
      sigfrac_A_l.setConstant();
      sigfrac_A_r.setConstant();
      sigfrac_B_l.setConstant();
      sigfrac_B_r.setConstant();
      sigma_A_l.setConstant();
      sigma_A_r.setConstant();
      sigma_B_l.setConstant();
      sigma_B_r.setConstant();
  
      //Step IV
      if(m_constraint){
        RooGaussian* fconstext_distance = new RooGaussian("fconstext_distance","fconstext_distance",length_A,RooConst(Distance),RooConst(SigmaConstraint)) ;
        model_eff->fitTo(*yeff_data, Minos(true), Range("leftA,rightA,leftB,rightB"), PrintLevel(-1), 
                         /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4), ExternalConstraints(*fconstext_distance));
      }else{
        model_eff->fitTo(*yeff_data, Minos(true), Range("leftA,rightA,leftB,rightB"), PrintLevel(-1), 
                           /*Minimizer("Minuit"),*/ ConditionalObservables(y), NumCPU(4));
      }
      
      length_A.setConstant(false);
      length_B.setConstant(false);
      N_A_l.setConstant(false);
      N_A_r.setConstant(false);
      N_B_l.setConstant(false);
      N_B_r.setConstant(false);
      sigfrac_A_l.setConstant(false);
      sigfrac_A_r.setConstant(false);
      sigfrac_B_l.setConstant(false);
      sigfrac_B_r.setConstant(false);
      sigma_A_l.setConstant(false);
      sigma_A_r.setConstant(false);
      sigma_B_l.setConstant(false);
      sigma_B_r.setConstant(false);
     
  
      cout << endl;
      cout << ModuleName << endl;
      cout << "> Center : " << center.getVal() << " " << center.getError() << endl;
      cout << "> Length : " << (length_A.getVal()+length_B.getVal())/2. << " ";
      cout << TMath::Sqrt(length_A.getError()*length_A.getError()+length_B.getError()*length_B.getError())/2. << endl;
      cout << "> Distance : " << TMath::Abs(distance.getVal()) << " " << distance.getError() << endl;
      cout << endl;
      
      m_YPosFitFile << ModuleName << " " << center.getVal() << " " << center.getError() << endl;
      m_LengthFitFile << ModuleName << " " << (length_A.getVal()+length_B.getVal())/2. << " ";
      m_LengthFitFile << TMath::Sqrt(length_A.getError()*length_A.getError()+length_B.getError()*length_B.getError())/2. << endl;
      m_DistanceFitFile << ModuleName << " " << TMath::Abs(distance.getVal()) << " " << distance.getError() << endl;
  
      m_FitParameterFile << ModuleName << " "<< center.getVal() << "+/-" << center.getError() << " ";
      m_FitParameterFile << length_A.getVal() << "+/-" << length_A.getError() << " ";
      m_FitParameterFile << length_B.getVal() << "+/-" << length_B.getError() << " ";
      m_FitParameterFile << distance.getVal() << "+/-" << distance.getError() << " ";
      m_FitParameterFile << sigma_A_l.getVal() << "+/-" << sigma_A_l.getError() << " ";
      m_FitParameterFile << sigma_A_r.getVal() << "+/-" << sigma_A_r.getError() << " ";
      m_FitParameterFile << sigma_B_l.getVal() << "+/-" << sigma_B_l.getError() << " ";
      m_FitParameterFile << sigma_B_r.getVal() << "+/-" << sigma_B_r.getError() << " ";
      m_FitParameterFile << endl;
  
    }

    TCanvas* canvas = new TCanvas("canvas","canvas",1920,1080);
    canvas->Divide(2,2);

    yeff_A_l->Print("v");  
    _model_eff_A_l->Print("v"); 
 
    canvas->cd(1);
    RooPlot* frame_A_l = y.frame(Range("leftA"));
    frame_A_l->SetTitle(ModuleName+_A);
    yeff_A_l->plotOn(frame_A_l,Efficiency(cat_eff));
    if(!m_glimpse){
      _model_eff_A_l->plotOn(frame_A_l);
      //model_eff_A_l->paramOn(frame_A_l, Layout(0.6,0.9,0.45));    
      //    if(!MC){ 
      //      if (ModuleName.Contains("M9") || ModuleName.Contains("M8") ) frame_A_l->SetMinimum(0.75);
      //      if( ModuleName.Contains("M7") ) frame_A_l->SetMinimum(0.5);
      //    }
    }
    frame_A_l->Draw();
    frame_A_l->GetXaxis()->SetTitle("Local Y [mm]");
    frame_A_l->GetYaxis()->SetTitle("Efficiency []");
    canvas->Update();

    yeff_B_l->Print("v"); 

    canvas->cd(3);
    RooPlot* frame_B_l = y.frame(Range("leftB"));
    frame_B_l->SetTitle(ModuleName+_B);
    yeff_B_l->plotOn(frame_B_l,Efficiency(cat_eff));
    if(!m_glimpse){
      _model_eff_B_l->plotOn(frame_B_l);
      //model_eff_B_l->paramOn(frame_B_l, Layout(0.6,0.9,0.45));    
      //    if(!MC){ 
      //    if(ModuleName.Contains("M9") || ModuleName.Contains("M8") ) frame_B_l->SetMinimum(0.75);
      //    if( ModuleName.Contains("M7") ) frame_B_l->SetMinimum(0.5);}
    }
    frame_B_l->Draw();
    frame_B_l->GetXaxis()->SetTitle("Local Y [mm]");
    frame_B_l->GetYaxis()->SetTitle("Efficiency []");
    canvas->Update();
    
    yeff_A_r->Print("v"); 
    
    canvas->cd(2);
    RooPlot* frame_A_r = y.frame(Range("rightA"));
    frame_A_r->SetTitle(ModuleName2+_A);
    yeff_A_r->plotOn(frame_A_r,Efficiency(cat_eff));
    if(!m_glimpse){
      _model_eff_A_r->plotOn(frame_A_r);
      //model_eff_A_r->paramOn(frame_A_r, Layout(0.6,0.9,0.45));    
      //    if(!MC){ 
      //    if(ModuleName2.Contains("M9") || ModuleName2.Contains("M8") ) frame_A_r->SetMinimum(0.75);
      //    if( ModuleName2.Contains("M7") ) frame_A_r->SetMinimum(0.5);}
    }
    frame_A_r->Draw();
    frame_A_r->GetXaxis()->SetTitle("Local Y [mm]");
    frame_A_r->GetYaxis()->SetTitle("Efficiency []");
    canvas->Update();
    
    yeff_B_r->Print("v"); 
    
    canvas->cd(4);
    RooPlot* frame_B_r = y.frame(Range("rightB"));
    frame_B_r->SetTitle(ModuleName2+_B);
    yeff_B_r->plotOn(frame_B_r,Efficiency(cat_eff));
    if(!m_glimpse){
      _model_eff_B_r->plotOn(frame_B_r);
      //model_eff_B_r->paramOn(frame_B_r, Layout(0.6,0.9,0.45));    
      //    if(!MC){ 
      //    if(ModuleName2.Contains("M9") || ModuleName2.Contains("M8") ) frame_B_r->SetMinimum(0.75);
      //    if( ModuleName2.Contains("M7") ) frame_B_r->SetMinimum(0.5);}
    }
    frame_B_r->Draw();
    frame_B_r->GetXaxis()->SetTitle("Local Y [mm]");
    frame_B_r->GetYaxis()->SetTitle("Efficiency []");
    canvas->Update();

    if(m_saveplots){
      boost::filesystem::path plotname(ModuleName+"_eff.eps");
      canvas->SaveAs( (outpath/plotpath/plotname).c_str() );
    }
    canvas->Close(); 
    
    //delete
    cout << "clean up RooDataSet & Model" << endl;
  }
  //clean up
  cout << "clean up Histograms" << endl;
  cout << "close file" << endl;
  if(datafile->IsOpen()) datafile->Close(); 

  return;
}

void OTYAlignMagOff::FitfromROOTFile()
{

  std::cout << " call function FitfromROOTFile() " << std::endl; 
 
  OTNames::Vector* sectors(m_Names->GetModules());
    
  OTNames::Vector::iterator It, Begin( sectors->begin() ), End( sectors->end() );
  
  for (It = Begin; It != End; It++)
  {
    TString tmp((*It).c_str());
    if(tmp.Contains("Q2") || tmp.Contains("Q3") || tmp.Contains("M1") /*|| tmp.Contains("M2") || tmp.Contains("M3") || tmp.Contains("M9")*/) continue;
    std::cout << " sector " << m_Names->GetRAWModuleName((*It)).c_str() << " to go " << std::endl; 
    GetMeanFromHisto(m_Names->GetRAWModuleName((*It)).c_str());
  }
  cout << "start clean up" << endl; 
  //clean up memory
  sectors->clear(); 
}


void OTYAlignMagOff::fit_efficiency()
{

  std::cout << " call function fit_efficiency() " << std::endl;
  // different parameter for simulated samples (MC2010)
  auto m_OT = boost::get<OTParam>(m_param);
  if( m_filename.Contains("MC") || m_OT.mc() ){
    m_OT.setMC(true);
    std::map<std::string,double> length   = boost::assign::map_list_of ("F", 36.) ("S1", 225) ("S23", 424) ("ALL",36);
    std::map<std::string,double> step     = boost::assign::map_list_of ("F", 0.) ("S1", 0) ("S23", 14) ("ALL",0);
    m_OT.setLength( length );
    m_OT.setStep( step );
  }
  
  RooMsgService::instance().setSilentMode(true);
  RooMsgService::instance().setStreamStatus(1,false);
  
  RooFit::Verbose(false);
    
  TString Titlename("Local Y Position");
  gROOT->SetStyle("Plain"); 
  gStyle->SetOptStat(0);
  gStyle->SetTitleX(0.5);
  gStyle->SetTitleAlign(23); 
  gStyle->SetTitleBorderSize(0);
  gStyle->SetPaintTextFormat("5.1f");
  gStyle->SetStatFontSize(0.15);
  gStyle->SetTitleFontSize(0.07);
    
  unsigned int nColors=52;
  int MyPalette[52];
  double s[3] = {0.00, 0.50, 1.00};
  double r[3] = {0.80, 1.00, 0.00};
  double g[3] = {0.00, 1.00, 0.00};
  double b[3] = {0.00, 1.00, 0.80};
  int FI = TColor::CreateGradientColorTable(3, s, r, g, b, nColors);
  for (unsigned int h(0); h < nColors; h++){
    MyPalette[h] = FI+h;
  }
  gStyle->SetNumberContours(nColors);
  gStyle->SetPalette(nColors, MyPalette);
  

  boost::filesystem::path outpath(m_outDirectory);
  boost::filesystem::path fitpath("fit");
  if(!boost::filesystem::exists(outpath/fitpath)) boost::filesystem::create_directories(outpath/fitpath);

  boost::filesystem::path yposfilename(m_outputname+".txt");
  boost::filesystem::path yposfile = outpath/fitpath/yposfilename;  
  m_YPosFitFile.open(yposfile.c_str());

  boost::filesystem::path lengthfilename(m_outputname+"_length.txt");
  boost::filesystem::path lengthfile = outpath/fitpath/lengthfilename;
  m_LengthFitFile.open(lengthfile.c_str());

  boost::filesystem::path distancefilename(m_outputname+"_distance.txt");
  boost::filesystem::path distancefile = outpath/fitpath/distancefilename;
  m_DistanceFitFile.open(distancefile.c_str());

  boost::filesystem::path fitparamtersfilename(m_outputname+"_fitparamters.txt");
  boost::filesystem::path fitparamtersfile = outpath/fitpath/fitparamtersfilename;
  m_FitParameterFile.open(fitparamtersfile.c_str());

  std::cout << " yposfile " << yposfile << std::endl;
  std::cout << "lengthfile "<< lengthfile << std::endl;
  std::cout << "distancefile " << distancefile << std::endl;
  std::cout << "fitparamtersfile " << fitparamtersfile << std::endl;

  std::cout << " read to call FitfromROOTFile() " << std::endl; 
 
  FitfromROOTFile();

  //clean up the memory
  m_YPosFitFile.close();     
  m_LengthFitFile.close();   
  m_DistanceFitFile.close(); 
  m_FitParameterFile.close(); 
  gDirectory->GetList()->Delete();

}

void OTYAlignMagOff::plots()
{
  Double_t Length_F(36.);
  Double_t Length_S1(200.);
  Double_t Length_S23(400.);
  Double_t Distance_F(36.);
  Double_t Distance_S1(0.);
  Double_t Distance_S23(0.);
  Double_t Step_S23(14.);
  Double_t Scale(6.);

  RooMsgService::instance().setSilentMode(kTRUE);
  RooMsgService::instance().setStreamStatus(1,kFALSE);

  auto m_OT = boost::get<OTParam>(m_param);
  if( m_OT.mc() ){
    Length_S1 = 225.;
    Length_S23 = 424.;
    Step_S23 = 14.;
  }
  
  boost::filesystem::path outpath(m_outDirectory);
  boost::filesystem::path plotpath("plots/"+m_outputname);
  if(!boost::filesystem::exists(outpath/plotpath)) boost::filesystem::create_directories(outpath/plotpath);
  
  TString Titlename_YPos("OT Y Position");
  TString Titlename_Length("OT Inactive Length");
  TString Titlename_Distance("OT Two Gaps Distance");

  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(0);
  gStyle->SetTitleX(0.5);
  gStyle->SetTitleAlign(23);
  gStyle->SetTitleBorderSize(0);
  gStyle->SetPaintTextFormat("5.1f");
  gStyle->SetStatFormat("5.5f");
  //gStyle->SetStatFontSize(0.15);
  gStyle->SetTitleFontSize(0.07);
  gStyle->SetPadTickY(1);
  gStyle->SetPadTickX(1);
  
  unsigned int nColors=52;
  Int_t MyPalette[52];
  Double_t s[3] = {0.00, 0.50, 1.00};
  Double_t b[3] = {0.80, 1.00, 0.00};
  Double_t g[3] = {0.00, 1.00, 0.00};
  Double_t r[3] = {0.00, 1.00, 0.80};
  Int_t FI = TColor::CreateGradientColorTable(3, s, r, g, b, nColors);
  for (unsigned int k(0); k < nColors; k++){
    MyPalette[k] = FI+k;
  }
  gStyle->SetNumberContours(nColors);
  gStyle->SetPalette(nColors, MyPalette);
  
  boost::filesystem::path fitpath("fit");
  boost::filesystem::path yposfilename(m_outputname+".txt");
  boost::filesystem::path yposfile = outpath/fitpath/yposfilename;  
  boost::filesystem::path lengthfilename(m_outputname+"_length.txt");
  boost::filesystem::path lengthfile = outpath/fitpath/lengthfilename;
  boost::filesystem::path distancefilename(m_outputname+"_distance.txt");
  boost::filesystem::path distancefile = outpath/fitpath/distancefilename;

  Parser* m_Parser = new Parser();
  Parser::Param_Result_Map* YPos     = m_Parser->CreateParamResultMapFromFitFile( yposfile.c_str() );
  Parser::Param_Result_Map* Length   = m_Parser->CreateParamResultMapFromFitFile( lengthfile.c_str() );
  Parser::Param_Result_Map* Distance = m_Parser->CreateParamResultMapFromFitFile( distancefile.c_str() );
 
  cout << " defined the parameters " << endl;

  OT2DPlot* h_YPos          = new OT2DPlot("h_YPos",         Titlename_YPos);
  OT2DPlot* h_Length        = new OT2DPlot("h_Length",       "Measured - Expected Gap Length");
  OT2DPlot* h_Length_F      = new OT2DPlot("h_Length_F",     "Measured - Expected Gap Length");
  OT2DPlot* h_Length_S1     = new OT2DPlot("h_Length_S1",    "Measured - Expected Gap Length");
  OT2DPlot* h_Length_S23_T1 = new OT2DPlot("h_Length_S23_T1","Measured - Expected Gap Length");
  OT2DPlot* h_Length_S23_T2 = new OT2DPlot("h_Length_S23_T2","Measured - Expected Gap Length");
  OT2DPlot* h_Length_S23_T3 = new OT2DPlot("h_Length_S23_T3","Measured - Expected Gap Length");
  OT2DPlot* h_Distance      = new OT2DPlot("h_Distance",     "Measured - Expected Two Gaps Distance");
  OT2DPlot* h_Distance_F    = new OT2DPlot("h_Distance_F",   "Measured - Expected Two Gaps Distance");
  OT2DPlot* h_Distance_S1   = new OT2DPlot("h_Distance_S1",  "Measured - Expected Two Gaps Distance");
  OT2DPlot* h_Distance_S23  = new OT2DPlot("h_Distance_S23", "Measured - Expected Two Gaps Distance");

  cout << " defined the OT2DPlot classes " << endl;

  gStyle->SetOptStat(1110);
  gStyle->SetStatY(0.87);
  gStyle->SetStatX(0.87);
  gStyle->SetStatH(0.3);
  gStyle->SetStatW(0.25);
  TH1D* h_YPos_          = new TH1D("h_YPos_",';'+Titlename_YPos+" [mm];",100,-Scale,Scale);
  TH1D* h_Length_F_      = new TH1D("h_Length_F_","F Modules;"+Titlename_Length+" [mm];",100,Length_F-Scale,Length_F+Scale+10);
  TH1D* h_Length_S1_     = new TH1D("h_Length_S1_","S1 Modules;"+Titlename_Length+" [mm];",100,Length_S1-Scale,Length_S1+Scale+20);
  TH1D* h_Length_S23_T1_ = new TH1D("h_Length_S23_T1_","S23 T1 Modules;"+Titlename_Length+" [mm];",100,Length_S23-Step_S23-Scale,Length_S23-Step_S23+Scale+20);
  TH1D* h_Length_S23_T2_ = new TH1D("h_Length_S23_T2_","S23 T2 Modules;"+Titlename_Length+" [mm];",100,Length_S23-Scale,Length_S23+Scale+20);
  TH1D* h_Length_S23_T3_ = new TH1D("h_Length_S23_T3_","S23 T3 Modules;"+Titlename_Length+" [mm];",100,Length_S23+Step_S23-Scale*2,Length_S23+Step_S23+Scale+20);
  TH1D* h_Distance_F_    = new TH1D("h_Distance_F_","F Modules;"+Titlename_Distance+" [mm];",100,Distance_F-Scale,Distance_F+Scale);
  TH1D* h_Distance_S1_   = new TH1D("h_Distance_S1_","S1 Modules;"+Titlename_Distance+" [mm];",100,Distance_S1-Scale,Distance_S1+Scale);
  TH1D* h_Distance_S23_  = new TH1D("h_Distance_S23_","S23 Modules;"+Titlename_Distance+" [mm];",100,Distance_S23-Scale,Distance_S23+Scale);
 
  cout << " defined the histograms TH1D " << endl;

  RooRealVar y("y","Local Y",-Scale,Scale,"mm");
  RooRealVar mean("mean","Mean",0.,-Scale*0.9,Scale*0.9,"mm");
  RooRealVar sigma("sigma","#sigma",0.5,0.,10.,"mm");
  RooGaussian* gauss = new RooGaussian("gauss","gauss",y,mean,sigma);
  
  std::map< std::string, TH1D* > hmap_YPos_Global;
  std::map< std::string, TH1D* > hmap_YPos_Station;
  std::map< std::string, TH1D* > hmap_YPos_Layer;
  
  OTNames::Vector* StationNames;
  StationNames = m_Names->GetStations();
  OTNames::Vector* LayerNames;
  LayerNames = m_Names->GetLayers();
  OTNames::ModuleNames* ModuleNames;
  
  string tmp;
  tmp = "h_YPos_";
  tmp += m_Names->GetGlobalName();
  hmap_YPos_Global[m_Names->GetGlobalName()] = new TH1D(tmp.c_str(),"",100,-Scale,Scale);
  
  for(OTNames::Vector::iterator It2 = StationNames->begin(); It2 != StationNames->end(); It2++)
  {
    tmp = "h_YPos_";
    tmp += (*It2);

    hmap_YPos_Station[*It2] = new TH1D(tmp.c_str(),"",100,-Scale,Scale);
  }
  
  for(OTNames::Vector::iterator It2 = LayerNames->begin(); It2 != LayerNames->end(); It2++)
  {
    tmp = "h_YPos_";
    tmp += (*It2);
    
    hmap_YPos_Layer[*It2] = new TH1D(tmp.c_str(),"",100,-Scale,Scale);
  }
  
  for(Parser::Param_Result_Map::const_iterator It = YPos->begin(); It != YPos->end(); It++)
  {
    ModuleNames = m_Names->GetModuleNames(It->first);
    
    if(!ModuleNames)
      continue;
    
    if(ModuleNames->Layer == 'U' || ModuleNames->Layer == 'V'){
      It->second->Value *= TMath::Cos(5.*TMath::Pi()/180.);
    }
    
  }
  
  for(Parser::Param_Result_Map::const_iterator It = YPos->begin(); It != YPos->end(); It++)
  {
    h_YPos->Fill('O'+It->first,It->second->Value,kTRUE);
    //std::cout << "Fill Y Position of " << It->first << std::endl;
    TString moduleName = It->first;
    //if( moduleName.Contains("X") )
    //if( !(moduleName.Contains("M9") || moduleName.Contains("M8") ) ){
    //std::cout << "module : " << moduleName << std::endl;
    h_YPos_->Fill(It->second->Value);
    hmap_YPos_Station[m_Names->GetStationName('O'+It->first)]->Fill(It->second->Value);
    hmap_YPos_Global[m_Names->GetGlobalName()]->Fill(It->second->Value);
    //}
  }
  
  cout << "######## Y Misalignment ########" << endl;
  
  if(hmap_YPos_Global["OT"]->GetEntries() > 0){
      cout << *fit_Gauss(hmap_YPos_Global["OT"],(RooGaussian*)gauss->Clone("gauss"),"OT",0.,Scale) << endl;
  }
  cout << endl;
  for(OTNames::Vector::iterator It2 = StationNames->begin(); It2 != StationNames->end(); It2++)
  {
    if(hmap_YPos_Station[*It2]->GetEntries() > 0){
      cout << *fit_Gauss(hmap_YPos_Station[*It2],(RooGaussian*)gauss->Clone("gauss"),(*It2).c_str(),0.,Scale) << endl;
    }
  }
  cout << endl;
  for(OTNames::Vector::iterator It2 = LayerNames->begin(); It2 != LayerNames->end(); It2++)
  {
    if(hmap_YPos_Layer[*It2]->GetEntries() > 0){
      cout << *fit_Gauss(hmap_YPos_Layer[*It2],(RooGaussian*)gauss->Clone("gauss"),(*It2).c_str(),0.,Scale) << endl;
    }
  }
  
  for(Parser::Param_Result_Map::const_iterator It = Length->begin(); It != Length->end(); It++)
  {
    ModuleNames = m_Names->GetModuleNames('O'+It->first);
    if(!ModuleNames)
      continue;
    
    if(ModuleNames->Moduleno == 9){
      if(ModuleNames->OTno == 1){
        h_Length->Fill('O'+It->first,It->second->Value-(Length_S23-Step_S23),kTRUE);
        h_Length_S23_T1->Fill('O'+It->first,It->second->Value,kTRUE);
        h_Length_S23_T1_->Fill(It->second->Value);
        h_Length_S23_T2->Fill('O'+It->first,0.0001,kTRUE);
        h_Length_S23_T3->Fill('O'+It->first,0.0001,kTRUE);
      }else if(ModuleNames->OTno == 2){
        h_Length->Fill('O'+It->first,It->second->Value-Length_S23,kTRUE);
        h_Length_S23_T2->Fill('O'+It->first,It->second->Value,kTRUE);
        h_Length_S23_T2_->Fill(It->second->Value);
        h_Length_S23_T1->Fill('O'+It->first,0.0001,kTRUE);
        h_Length_S23_T3->Fill('O'+It->first,0.0001,kTRUE);
      }else{
        h_Length->Fill('O'+It->first,It->second->Value-(Length_S23+Step_S23),kTRUE);
        h_Length_S23_T3->Fill('O'+It->first,It->second->Value,kTRUE);
        h_Length_S23_T3_->Fill(It->second->Value);
        h_Length_S23_T1->Fill('O'+It->first,0.0001,kTRUE);
        h_Length_S23_T2->Fill('O'+It->first,0.0001,kTRUE);
      }
      h_Length_F->Fill('O'+It->first,0.0001,kTRUE);
      h_Length_S1->Fill('O'+It->first,0.0001,kTRUE);
    }else if(ModuleNames->Moduleno == 8){
      h_Length->Fill('O'+It->first,It->second->Value-Length_S1,kTRUE);
      h_Length_S1->Fill('O'+It->first,It->second->Value,kTRUE);
      h_Length_S1_->Fill(It->second->Value);
      h_Length_F->Fill('O'+It->first,0.0001,kTRUE);
      h_Length_S23_T1->Fill('O'+It->first,0.0001,kTRUE);
      h_Length_S23_T2->Fill('O'+It->first,0.0001,kTRUE);
      h_Length_S23_T3->Fill('O'+It->first,0.0001,kTRUE);
    }else if(ModuleNames->Moduleno < 8){
      h_Length->Fill('O'+It->first,It->second->Value-Length_F,kTRUE);
      h_Length_F->Fill('O'+It->first,It->second->Value,kTRUE);
      
      TString moduleName = It->first;
      //if( moduleName.Contains("X") )
        h_Length_F_->Fill(It->second->Value);      
      h_Length_S1->Fill('O'+It->first,0.0001,kTRUE);
      h_Length_S23_T1->Fill('O'+It->first,0.0001,kTRUE);
      h_Length_S23_T2->Fill('O'+It->first,0.0001,kTRUE);
      h_Length_S23_T3->Fill('O'+It->first,0.0001,kTRUE);
    }
  }
  
  for(Parser::Param_Result_Map::const_iterator It = Distance->begin(); It != Distance->end(); It++)
  {
    ModuleNames = m_Names->GetModuleNames('O'+It->first);
    if(!ModuleNames)
      continue;
    if(ModuleNames->Moduleno == 9){
      h_Distance->Fill('O'+It->first,It->second->Value-Distance_S23,kTRUE);
      h_Distance_S23->Fill('O'+It->first,It->second->Value,kTRUE);
      h_Distance_S23_->Fill(It->second->Value);
      h_Distance_F->Fill('O'+It->first,0.0001,kTRUE);
      h_Distance_S1->Fill('O'+It->first,0.0001,kTRUE);
    }else if(ModuleNames->Moduleno == 8){
      h_Distance->Fill('O'+It->first,It->second->Value-Distance_S1,kTRUE);
      h_Distance_S1->Fill('O'+It->first,It->second->Value,kTRUE);
      h_Distance_S1_->Fill(It->second->Value);
      h_Distance_F->Fill('O'+It->first,0.0001,kTRUE);
      h_Distance_S23->Fill('O'+It->first,0.0001,kTRUE);
    }else if(ModuleNames->Moduleno < 8){
      h_Distance->Fill('O'+It->first,It->second->Value-Distance_F,kTRUE);
      h_Distance_F->Fill('O'+It->first,It->second->Value,kTRUE);
      TString moduleName = It->first;
      //if( moduleName.Contains("X") )
        h_Distance_F_->Fill(It->second->Value);
      h_Distance_S1->Fill('O'+It->first,-100000.,kTRUE);
      h_Distance_S23->Fill('O'+It->first,-100000.,kTRUE);
    }
  }

  cout << "######## Inactive Length ########" << endl;

  cout << "######## Two Gaps Distance ########" << endl;

  TPaveText *lhcb7TeVPrelimR = new TPaveText(0.17, 0.75, 0.44, 0.88, "BRNDC");
  lhcb7TeVPrelimR->SetFillColor(0);
  lhcb7TeVPrelimR->SetTextAlign(12);
  lhcb7TeVPrelimR->SetBorderSize(0);
  lhcb7TeVPrelimR->AddText("LHCb Preliminary 2012");
  
  cout << "######## TEST 1 ########" << endl;
  TCanvas* c_YPos = new TCanvas("c_YPos","c_YPos",1200,1000);
  
  h_YPos_->SetTitle(Titlename_YPos);
  h_YPos_->SetFillStyle(3004);
  h_YPos_->SetFillColor(2);
  h_YPos_->SetLineColor(2);
  h_YPos_->Draw();
  //lhcb7TeVPrelimR->Draw();
  cout << "######## TEST 2 ########" << endl;
  
  //c_YPos->SaveAs("plots/"+outputname+".eps");
  boost::filesystem::path plotname(m_outputname+".eps");
  c_YPos->SaveAs( (outpath/plotpath/plotname).c_str() );

  TCanvas* c_YPos_2D = new TCanvas("c_YPos_2D","c_YPos_2D",1200,600);
  
  h_YPos->Histogram()->SetMinimum(-Scale);
  h_YPos->Histogram()->SetMaximum(Scale);
  h_YPos->Draw("COLZ");
  h_YPos->Draw("TEXT,same");
    
  cout << "######## TEST 3 ########" << endl;
  //c_YPos_2D->SaveAs("plots/"+outputname+"_2D.eps");
  plotname = boost::filesystem::path(m_outputname+"_2D.eps");
  c_YPos_2D->SaveAs( (outpath/plotpath/plotname).c_str() );
  
  TCanvas* c_Length = new TCanvas("c_Length","c_Length",1200,600);
  c_Length->Divide(1,2);
  
  c_Length->cd(1);
  TPad* p_Length = static_cast<TPad*>(gPad);
  p_Length->Divide(2,1);
  p_Length->cd(1);
  h_Length_F_->Draw();
  h_Length_F_->SetFillStyle(3004);
  h_Length_F_->SetFillColor(2);
  h_Length_F_->SetLineColor(2);
  TLine* line_Length_F = new TLine(Length_F,0,Length_F,h_Length_F_->GetMaximum()*0.95);
  line_Length_F->SetLineColor(kBlue);
  line_Length_F->SetLineWidth(3.);
  line_Length_F->Draw();
  //lhcb7TeVPrelimR->Draw();
  p_Length->cd(2);
  h_Length_S1_->Draw();
  TLine* line_Length_S1 = new TLine(Length_S1,0,Length_S1,h_Length_S1_->GetMaximum()*0.95);
  line_Length_S1->SetLineColor(kBlue);
  line_Length_S1->SetLineWidth(3.);
  line_Length_S1->Draw();
  //lhcb7TeVPrelimR->Draw();
  h_Length_S1_->SetFillStyle(3004);
  h_Length_S1_->SetFillColor(2);
  h_Length_S1_->SetLineColor(2);
  c_Length->cd(2);
  TPad* p_Length_Sub = static_cast<TPad*>(gPad);
  p_Length_Sub->Divide(3,1);
  p_Length_Sub->cd(1);
  h_Length_S23_T1_->Draw();
  h_Length_S23_T1_->SetFillStyle(3004);
  h_Length_S23_T1_->SetFillColor(2);
  h_Length_S23_T1_->SetLineColor(2);
  TLine* line_Length_S23_T1 = new TLine(Length_S23-Step_S23,0,Length_S23-Step_S23,h_Length_S23_T1_->GetMaximum()*0.95);
  line_Length_S23_T1->SetLineColor(kBlue);
  line_Length_S23_T1->SetLineWidth(3.);
  line_Length_S23_T1->Draw();
  //lhcb7TeVPrelimR->Draw();
  p_Length_Sub->cd(2);
  h_Length_S23_T2_->Draw();
  TLine* line_Length_S23_T2 = new TLine(Length_S23,0,Length_S23,h_Length_S23_T2_->GetMaximum()*0.95);
  line_Length_S23_T2->SetLineColor(kBlue);
  line_Length_S23_T2->SetLineWidth(3.);
  line_Length_S23_T2->Draw();
  //lhcb7TeVPrelimR->Draw();
  h_Length_S23_T2_->SetFillStyle(3004);
  h_Length_S23_T2_->SetFillColor(2);
  h_Length_S23_T2_->SetLineColor(2);
  p_Length_Sub->cd(3);
  h_Length_S23_T3_->Draw();
  TLine* line_Length_S23_T3 = new TLine(Length_S23+Step_S23,0,Length_S23+Step_S23,h_Length_S23_T3_->GetMaximum()*0.95);
  line_Length_S23_T3->SetLineColor(kBlue);
  line_Length_S23_T3->SetLineWidth(3.);
  line_Length_S23_T3->Draw();
  //lhcb7TeVPrelimR->Draw();
  h_Length_S23_T3_->SetFillStyle(3004);
  h_Length_S23_T3_->SetFillColor(2);
  h_Length_S23_T3_->SetLineColor(2);
  
  //c_Length->SaveAs("plots/"+outputname+"_length.eps");
  plotname = boost::filesystem::path(m_outputname+"_length.eps");
  c_Length->SaveAs( (outpath/plotpath/plotname).c_str() );

  TCanvas* c_Length_2D = new TCanvas("c_Length_2D","c_Length_2D",1200,600);

  h_Length->Histogram()->SetMinimum(-Scale);
  h_Length->Histogram()->SetMaximum(+Scale);
  h_Length->Draw("COLZ");
  h_Length->Draw("TEXT,same");

  //c_Length_2D->SaveAs("plots/"+outputname+"_length_2D.eps");
  plotname = boost::filesystem::path(m_outputname+"_length_2D.eps");
  c_Length_2D->SaveAs( (outpath/plotpath/plotname).c_str() );

  TCanvas* c_Distance = new TCanvas("c_Distance","c_Distance",1800,600);
  
  TPad* p_Distance = static_cast<TPad*>(gPad);
  p_Distance->Divide(3,1);
  p_Distance->cd(1);
  h_Distance_F_->Draw();
  h_Distance_F_->SetFillStyle(3004);
  h_Distance_F_->SetFillColor(2);
  h_Distance_F_->SetLineColor(2);
  TLine* line_Distance_F = new TLine(Distance_F,0,Distance_F,h_Distance_F_->GetMaximum()*0.95);
  line_Distance_F->SetLineColor(kBlue);
  line_Distance_F->SetLineWidth(3.);
  line_Distance_F->Draw();
  //lhcb7TeVPrelimR->Draw();
  p_Distance->cd(2);
  h_Distance_S1_->Draw();
  TLine* line_Distance_S1 = new TLine(Distance_S1,0,Distance_S1,h_Distance_S1_->GetMaximum()*0.95);
  line_Distance_S1->SetLineColor(kBlue);
  line_Distance_S1->SetLineWidth(3.);
  line_Distance_S1->Draw();
  //lhcb7TeVPrelimR->Draw();
  h_Distance_S1_->SetFillStyle(3004);
  h_Distance_S1_->SetFillColor(2);
  h_Distance_S1_->SetLineColor(2);
  p_Distance->cd(3);
  h_Distance_S23_->Draw();
  TLine* line_Distance_S23 = new TLine(Distance_S23,0,Distance_S23,h_Distance_S23_->GetMaximum()*0.95);
  line_Distance_S23->SetLineColor(kBlue);
  line_Distance_S23->SetLineWidth(3.);
  line_Distance_S23->Draw();
  //lhcb7TeVPrelimR->Draw();
  h_Distance_S23_->SetFillStyle(3004);
  h_Distance_S23_->SetFillColor(2);
  h_Distance_S23_->SetLineColor(2);
    
  //c_Distance->SaveAs("plots/"+outputname+"_distance.eps");
  plotname = boost::filesystem::path(m_outputname+"_distance.eps");
  c_Distance->SaveAs( (outpath/plotpath/plotname).c_str() );

  TCanvas* c_Distance_2D = new TCanvas("c_Distance_2D","c_Distance_2D",1200,600);

  h_Distance->Histogram()->SetMinimum(-Scale);
  h_Distance->Histogram()->SetMaximum(+Scale);
  h_Distance->Draw("COLZ");
  h_Distance->Draw("TEXT,same");

  //c_Distance_2D->SaveAs("plots/"+outputname+"_distance_2D.eps");
  plotname = boost::filesystem::path(m_outputname+"_distance_2D.eps");
  c_Distance_2D->SaveAs( (outpath/plotpath/plotname).c_str() );
}

